package com.smg.visitor.config.filter;

import com.smg.visitor.dto.request.admin.auth.AuthProfileRequest;
import com.smg.visitor.postgressource.entity.PgUser;
import com.smg.visitor.services.admin.AdminAuthService;
import com.smg.visitor.util.EncryptDecryptProvider;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Order(102)
public class AuthTokenFilter extends OncePerRequestFilter {

    @Autowired
    private AdminAuthService authService;

    @Autowired
    private EncryptDecryptProvider encryptDecryptProvider;

    /**
     * @param httpServletRequest
     * @param httpServletResponse
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String tokenHeader = httpServletRequest.getHeader("Authentication");
        if (tokenHeader != null && tokenHeader.split(" ").length > 1) {
            tokenHeader = tokenHeader.replace("Bearer ", "");
            tokenHeader = encryptDecryptProvider.Decrypt(tokenHeader);
            String[] data = tokenHeader.split("-");
            if (data.length > 1) {
                PgUser pgUser = authService.verifyAuthToken(data[0]);
                if (pgUser != null) {
//                    List<SimpleGrantedAuthority> authorities = this.rolePermissions(pgUser.getId());
                    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    authorities.add(new SimpleGrantedAuthority(RoleKeyEnum.ROLE_ADMIN));
                    AuthProfileRequest authProfileRequest = new AuthProfileRequest();
                    authProfileRequest.setId(pgUser.getId());
                    authProfileRequest.setEmail(pgUser.getEmail());
                    authProfileRequest.setName(pgUser.getName());
                    authProfileRequest.setEnable(pgUser.isEnable());
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(authProfileRequest, null, authorities);
                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
