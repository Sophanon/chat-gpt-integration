package com.smg.visitor.config.exception;

public class ResponseException extends Exception{

    int code = 400;

    public ResponseException(String message) {
        super(message);
    }

    public ResponseException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
