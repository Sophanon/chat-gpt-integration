package com.smg.visitor.config.exception;

import lombok.Data;
import org.springframework.http.ResponseEntity;

import java.util.List;

@Data
public class ApiError {
    private int code;
    private String message;
    private Object errors;
    private Object validation;


    public ApiError(int code, String message,Object errors) {
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

    public ApiError(int code, String message,Object errors, Object validation) {
        this.code = code;
        this.message = message;
        this.errors = errors;
        this.validation = validation;
    }

    public static ResponseEntity<?> data(int code, String message, Object errors){
        return ResponseEntity.status(code).body(new ApiError(code, message, errors));
    }
}
