package com.smg.visitor.component;

import com.smg.visitor.postgressource.entity.*;
import com.smg.visitor.postgressource.repository.*;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class Dataloader implements CommandLineRunner {

    @Autowired
    private PgUserRepository pgUserRepository;

    @Autowired
    private PgRoleRepository pgRoleRepository;

    @Autowired
    private PgPermissionRepository pgPermissionRepository;

    @Autowired
    private PgRolePermissionMapRepository pgRolePermissionMapRepository;

    @Autowired
    private PgUserRoleMapRepository pgUserRoleMapRepository;

    @Autowired
    private PgSystemParamRepository pgSystemParamRepository;

    @Override
    public void run(String... args) throws Exception {
        this.loadUser();
        this.loadPermission();
        this.loadRole();
        this.loadUserRole();
        this.loadSystemParams();
    }

    public void loadUser()
    {
        Optional<PgUser> pgUserOptional = pgUserRepository.findTopByEmail("admin@prasac.com");
        if (pgUserOptional.isEmpty()) {
            PgUser pgUser = new PgUser();
            pgUser.setId(1);
            pgUser.setName("admin");
            pgUser.setEmail("admin@prasac.com");
            pgUser.setPassword("password");
            pgUser.setUserName("admin");
            pgUser.setEnable(true);
            pgUser.setAdAccount(false);
            pgUserRepository.save(pgUser);
        }
    }

    private void loadPermission() {
        String[] permissions = {
                "Dashboard,dashboard,[read]",
                "Administrator,administrator,[read]|[create]|[update]|[delete]",
                "Role Permission,role-permission,[read]|[create]|[update]|[delete]",
                "System Param,system-param,[read]|[update]",
        };

        for (String row : permissions) {
            String[] data = row.split(",");
            PgPermission permission = new PgPermission();
            Optional<PgPermission> pgPermissionOptional = pgPermissionRepository.findByCode(data[1]);
            if (pgPermissionOptional.isPresent()) {
                permission = pgPermissionOptional.get();
            }
            permission.setName(data[0]);
            permission.setCode(data[1]);
            permission.setActions(data[2]);
            permission.setModule(data.length > 3 ? data[3] : "");
            permission.setSequenceOrder(ArrayUtils.indexOf(permissions, row));

            pgPermissionRepository.save(permission);
        }
    }

    private void loadRole() throws JSONException {
        PgRole role = new PgRole();
        Optional<PgRole> pgRoleOptional = pgRoleRepository.findById(1);
        if (pgRoleOptional.isPresent()) {
            role = pgRoleOptional.get();
        }
        role.setName("Super Admin");
        role.setId(1);
        role.setDescription("Super admin can do anything");
        role.setEnable(true);
        pgRoleRepository.save(role);
        pgRolePermissionMapRepository.deleteAllByRoleId(1);
        List<PgPermission> permissions = pgPermissionRepository.findAll();
        permissions.forEach(permission -> {
            PgRolePermissionMap pgRolePermissionMap = new PgRolePermissionMap();
            pgRolePermissionMap.setRoleId(1);
            pgRolePermissionMap.setPermissionId(permission.getId());
            pgRolePermissionMap.setActions(permission.getActions());
            pgRolePermissionMapRepository.save(pgRolePermissionMap);
        });
    }

    private void loadUserRole() {
        Optional<PgUser> pgUserOptional = pgUserRepository.findTopByEmail("admin@prasac.com");
        if (pgUserOptional.isPresent()) {
            if (pgUserRoleMapRepository.findTopByUserIdAndRoleId(pgUserOptional.get().getId(), 1).isEmpty()) {
                pgUserRoleMapRepository.deleteByUserId(pgUserOptional.get().getId());
                PgUserRoleMap pgUserRoleMap = new PgUserRoleMap();
                pgUserRoleMap.setUserId(pgUserOptional.get().getId());
                pgUserRoleMap.setRoleId(1);
                pgUserRoleMapRepository.save(pgUserRoleMap);
            }
        }
    }

    public void loadSystemParams() {
        List<PgSystemParam> pgSystemParams = new ArrayList<>();
        if (pgSystemParamRepository.findTopByKey("access_token_expired_at").isEmpty()) {
            pgSystemParams.add(new PgSystemParam("access_token_expired_at", "100"));
        }
        if (pgSystemParamRepository.findTopByKey("refresh_token_expired_at").isEmpty()) {
            pgSystemParams.add(new PgSystemParam("refresh_token_expired_at", "1000"));
        }
        if (pgSystemParamRepository.findTopByKey("aws_url").isEmpty()) {
            pgSystemParams.add(new PgSystemParam("aws_url", "https://sophanonchhoun.s3.ap-southeast-1.amazonaws.com"));
        }
        if (pgSystemParamRepository.findTopByKey("chatgpt_url").isEmpty()) {
            pgSystemParams.add(new PgSystemParam("chatgpt_url", "https://api.openai.com/v1/chat/completions"));
        }
        this.pgSystemParamRepository.saveAll(pgSystemParams);
    }

    public String randomText()
    {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }

}
