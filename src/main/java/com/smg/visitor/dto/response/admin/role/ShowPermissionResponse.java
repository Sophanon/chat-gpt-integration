package com.smg.visitor.dto.response.admin.role;

import lombok.Data;

@Data
public class ShowPermissionResponse {
    private int id;
    private String code;
    private String name;
    private String actions;
    private String[] availableActions;
    private String[] permissionAction;
}
