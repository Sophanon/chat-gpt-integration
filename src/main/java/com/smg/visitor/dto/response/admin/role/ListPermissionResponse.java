package com.smg.visitor.dto.response.admin.role;

import com.smg.visitor.util.CoreBase;
import lombok.Data;

import java.util.Collections;

@Data
public class ListPermissionResponse {

    private int id;
    private String code;
    private String name;
    private String actions;
    private String[] availableActions;

    private Object permissionAction;

    public Object getPermissionAction() {
        return Collections.emptyList();
    }

    public String[] getAvailableActions() {
        return CoreBase.getAvailableActions(this.getActions());
    }

}
