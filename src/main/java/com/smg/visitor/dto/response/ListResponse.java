package com.smg.visitor.dto.response;
import lombok.Data;

import java.io.Serializable;

@Data
public class ListResponse implements Serializable {
    private Object list;
    private Long total;

    public ListResponse(Object list, Long total){
        this.list = list;
        this.total = total;
    }
}
