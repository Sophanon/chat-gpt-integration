package com.smg.visitor.dto.response.admin.role;

import com.smg.visitor.postgressource.entity.PgRole;
import com.smg.visitor.util.CoreBase;
import lombok.Data;

import java.util.List;

@Data
public class ShowRoleResponse {

    private int id;
    private String name;
    private String description;
    private List<ShowPermissionResponse> abilities;
    private boolean isEnable;

    public ShowRoleResponse(PgRole pgRole) {
        this.id = pgRole.getId();
        this.name = pgRole.getName();
        this.description = pgRole.getDescription();
        this.isEnable = pgRole.isEnable();
        this.abilities = CoreBase.getRoleAbilities(pgRole.getId());
    }

}
