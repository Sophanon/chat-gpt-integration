package com.smg.visitor.dto.response.admin.user;

import com.smg.visitor.postgressource.entity.PgUser;
import lombok.Data;

@Data
public class ListUserResponse {
    private int id;
    private String name;

    private String userName;
    private String email;
    private boolean isEnable;

    public ListUserResponse(PgUser pgUser) {
        this.id = pgUser.getId();
        this.name = pgUser.getName();
        this.userName = pgUser.getUserName();
        this.email = pgUser.getEmail();
        this.isEnable = pgUser.isEnable();
    }

}
