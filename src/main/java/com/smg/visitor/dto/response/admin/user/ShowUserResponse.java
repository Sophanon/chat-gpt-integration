package com.smg.visitor.dto.response.admin.user;


import com.smg.visitor.dto.response.admin.role.ListRoleOptionResponse;
import com.smg.visitor.postgressource.entity.PgUser;
import com.smg.visitor.util.CoreBase;
import lombok.Data;

import java.util.List;


@Data
public class ShowUserResponse {
    private int id;
    private String name;

    private String userName;
    private String email;
    private boolean isEnable;

    private List<ListRoleOptionResponse> roles;

    public ShowUserResponse(PgUser pgUser) {
        this.id = pgUser.getId();
        this.name = pgUser.getName();
        this.userName = pgUser.getUserName();
        this.email = pgUser.getEmail();
        this.isEnable = pgUser.isEnable();
        this.roles = CoreBase.getRoleByUserId(this.id);
    }


}
