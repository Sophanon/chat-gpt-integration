package com.smg.visitor.dto.response.admin.system_param;

import com.smg.visitor.postgressource.entity.PgSystemParam;
import lombok.Data;

import java.util.Optional;

@Data
public class ListSystemParamOptionResponse {

    private String key;
    private String name;

    public ListSystemParamOptionResponse(Optional<PgSystemParam> pgSystemParamOptional, String name) {
        this.name = name;
        pgSystemParamOptional.ifPresent(pgSystemParam -> this.key = pgSystemParam.getKey());
    }

}
