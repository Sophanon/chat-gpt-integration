package com.smg.visitor.dto.response.admin.system_param;

import com.smg.visitor.postgressource.entity.PgSystemParam;
import lombok.Data;

@Data
public class ListSystemParamResponse {

    private int id;
    private String key;

    private String value;

    public ListSystemParamResponse(PgSystemParam pgSystemParam) {
        this.id = pgSystemParam.getId();
        this.key = pgSystemParam.getKey();
        this.value = pgSystemParam.getValue();
    }


}
