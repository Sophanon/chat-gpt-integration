package com.smg.visitor.dto.response.admin.auth;

import com.smg.visitor.postgressource.entity.PgUser;
import com.smg.visitor.util.CoreBase;
import lombok.Data;
import org.json.JSONArray;

@Data
public class UserProfileResponse {

    private int id;
    private String email;
    private String name;
    private boolean isEnable;

    private String userName;

    private Object ability;


    public UserProfileResponse(PgUser pgUser) {
        this.id = pgUser.getId();
        this.email = pgUser.getEmail();
        this.name = pgUser.getName();
        this.isEnable = pgUser.isEnable();
        this.userName = pgUser.getUserName();
        this.ability = CoreBase.getUserAbilities(this.getId());
    }

}
