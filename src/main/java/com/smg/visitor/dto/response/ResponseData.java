package com.smg.visitor.dto.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class ResponseData implements Serializable {

    private int code = 200;
    private String message = "";
    private Object data = "";


    public ResponseData(int code, Object data, String message){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ResponseEntity<?> data(int code, Object data, String message){
        return ResponseEntity.status(code).body(new ResponseData(code, data, message));
    }

    public ResponseData(Object data) {
        this.data = data;
    }

}
