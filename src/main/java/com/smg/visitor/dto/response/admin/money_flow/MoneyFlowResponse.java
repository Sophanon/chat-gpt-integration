package com.smg.visitor.dto.response.admin.money_flow;

import com.smg.visitor.postgressource.entity.PgMoneyFlow;
import lombok.Data;

import java.time.LocalDate;

@Data
public class MoneyFlowResponse {

    private int id;
    private String type;
    private String description;
    private double amount;
    private LocalDate date;

    public MoneyFlowResponse(PgMoneyFlow pgMoneyFlow) {
        this.id = pgMoneyFlow.getId();
        this.type = pgMoneyFlow.getType();
        this.description = pgMoneyFlow.getDescription();
        this.amount = pgMoneyFlow.getAmount();
        this.date = pgMoneyFlow.getDate();
    }

}
