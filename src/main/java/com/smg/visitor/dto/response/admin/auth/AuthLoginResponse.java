package com.smg.visitor.dto.response.admin.auth;

import com.smg.visitor.postgressource.entity.PgUserLoginAccess;
import com.smg.visitor.util.CoreBase;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AuthLoginResponse {


    private int userId;
    private String accessToken;
    private String refreshToken;
    private LocalDateTime accessTokenExpiredAt;
    private LocalDateTime refreshTokenExpiredAt;
    private UserProfileResponse user;

    public AuthLoginResponse(PgUserLoginAccess pgUserLoginAccess) throws Exception {
        this.userId = pgUserLoginAccess.getUserId();
        this.accessToken = pgUserLoginAccess.getAccessToken();
        this.refreshToken = pgUserLoginAccess.getRefreshToken();
        this.accessTokenExpiredAt = pgUserLoginAccess.getAccessTokenExpiredAt();
        this.refreshTokenExpiredAt = pgUserLoginAccess.getRefreshTokenExpiredAt();
        this.user = CoreBase.getUserById(this.userId);
    }


}
