package com.smg.visitor.dto.response.admin.role;

import com.smg.visitor.postgressource.entity.PgRole;
import lombok.Data;

@Data
public class ListRoleResponse {

    private int id;
    private String name;
    private String description;
    private boolean isEnable;

    public ListRoleResponse(PgRole role)
    {
        this.id = role.getId();
        this.name =role.getName();
        this.description = role.getDescription();
        this.isEnable = role.isEnable();
    }

}
