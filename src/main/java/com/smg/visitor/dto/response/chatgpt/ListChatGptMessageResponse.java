package com.smg.visitor.dto.response.chatgpt;

import com.smg.visitor.postgressource.entity.PgChatTelegram;
import lombok.Data;

@Data
public class ListChatGptMessageResponse {

    private String role;
    private String content;

    public ListChatGptMessageResponse(PgChatTelegram pgChatTelegram)
    {
        this.role = pgChatTelegram.getRole();
        this.content = pgChatTelegram.getContent();
    }

}
