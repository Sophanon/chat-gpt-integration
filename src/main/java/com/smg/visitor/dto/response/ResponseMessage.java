package com.smg.visitor.dto.response;

public interface ResponseMessage {

    String LOGIN_SUCCESS = "You have successfully logged.";
    String UNAUTHORIZED = "Unauthorized";
    String LOGOUT_SUCCESS = "You have successfully logged out.";
    String BAD_CREDENTIAL = "Incorrect, please enter your username and password!";
    String ACCOUNT_NOT_FOUND= "This account is not found.";
    String ACCOUNT_BLOCK = "This account is blocked";

    String WRONG_DESTINATION = "Not found.";

    String INTERNAL_SERVER_ERROR = "Something went wrong.";

    String BAD_REQUEST = "Bad Request";

    String UNSUCCESS = "Not Success";

    String success = "Success";
    String CREATED_SUCCESS = "Created successfully";
    String UPDATED_SUCCESS = "Updated successfully";

    String ROLE_ALREADY_EXIST = "This role already exist.";
    String USER_ALREADY_EXIST = "This user already exist.";
    String USER_NAME_ALREADY_EXIST = "This user name already exist.";

    String ITEM_NOT_FOUND = "This item is not found.";
    String EMAIL_ALREADY_EXIST = "This email is already exist.";

    String FORBIDDEN = "You don't have permission to access on this.";

    String CANNOT_TURN_OFF = "You cannot turn your own status off.";

    String CANNOT_DELETE = "You cannot delete yourselves.";

    String ERROR_UPLOAD_TO_MB_SERVICE = "Error upload to mb service, please check.";

    String OLD_PASSWORD_NOT_CORRECT = "Your old password is not correct.";

}
