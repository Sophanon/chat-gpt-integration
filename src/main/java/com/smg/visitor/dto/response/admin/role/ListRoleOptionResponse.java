package com.smg.visitor.dto.response.admin.role;

import com.smg.visitor.postgressource.entity.PgRole;
import lombok.Data;

@Data
public class ListRoleOptionResponse {

    private int id;
    private String name;

    public ListRoleOptionResponse(PgRole role) {
        this.id = role.getId();
        this.name = role.getName();
    }

    public ListRoleOptionResponse(){}
}
