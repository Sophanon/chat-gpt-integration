package com.smg.visitor.dto.response.file;

import com.smg.visitor.postgressource.entity.PgMedia;
import com.smg.visitor.util.CoreBase;
import lombok.Data;

@Data
public class ListFileResponse {

    private int id;
    private String name;
    private String path;
    private String extension;
    private String url;

    public ListFileResponse(PgMedia pgMedia)
    {
        this.id = pgMedia.getId();
        this.name = pgMedia.getName();
        this.path = pgMedia.getPath();
        this.extension = pgMedia.getExtension();
    }

}
