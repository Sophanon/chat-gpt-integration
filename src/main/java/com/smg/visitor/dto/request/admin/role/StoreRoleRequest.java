package com.smg.visitor.dto.request.admin.role;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class StoreRoleRequest {

    @NotEmpty
    private String name;

    private String description;

    @NotEmpty
    private List<StorePermissionRequest> abilities;

    private boolean isEnable;

}
