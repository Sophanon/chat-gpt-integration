package com.smg.visitor.dto.request.admin.user;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StoreRoleRequest {
    @NotNull
    private int id;
    private String name;
}
