package com.smg.visitor.dto.request.admin.auth;

import javax.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
public class AdminAuthLoginRequest implements Serializable {

    @NotEmpty(message = "email is required.")
    private String email;

    @NotEmpty(message = "password is required.")
    private String password;

}
