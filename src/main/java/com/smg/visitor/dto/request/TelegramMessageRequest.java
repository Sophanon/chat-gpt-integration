package com.smg.visitor.dto.request;

import lombok.Data;

@Data
public class TelegramMessageRequest {
    private Text text;
    private long update_id;

    private Message message;



    @Data
    public static class Text {
        private String update_id;
        private Message message;

    }

    @Data
    public static class Message {
        private int message_id;
        private From from;
        private Chat chat;
        private int date;
        private String text;

        private NewChatParticipant new_chat_participant;

        @Data
        public static class NewChatParticipant {
            private long id;
            private boolean is_bot;
            private String first_name;
            private String username;
        }

        @Data
        public static class From {
            private int id;
            private boolean is_bot;
            private String first_name;
            private String last_name;
            private String username;
        }
        @Data
        public static class Chat {
            private long id;
            private String title;
            private String type;
        }
    }
}
