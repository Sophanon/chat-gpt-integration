package com.smg.visitor.dto.request.admin.auth;

import lombok.Data;

@Data
public class AuthProfileRequest {

    private int id;
    private String email;
    private String name;
    private boolean isEnable;

    private String refreshToken;


    private String error;
}
