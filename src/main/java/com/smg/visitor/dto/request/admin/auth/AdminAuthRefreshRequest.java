package com.smg.visitor.dto.request.admin.auth;

import lombok.Data;

@Data
public class AdminAuthRefreshRequest {

    private String token;


}
