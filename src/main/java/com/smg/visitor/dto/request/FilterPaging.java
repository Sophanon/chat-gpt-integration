package com.smg.visitor.dto.request;

import lombok.Data;
import org.apache.commons.text.CaseUtils;


@SuppressWarnings("serial")
@Data
public class FilterPaging {
    private String searchBy= "";
    private String searchOperator= ""; // like, equal
    private Object search;

    public String getSearchBy() {
        if (defaultCase) {
            return searchBy;
        }
        return CaseUtils.toCamelCase(searchBy, false, '_');
    }

    private boolean defaultCase = false;

    public FilterPaging(String searchBy, String searchOperator, Object search){
        this.setSearchBy(searchBy);
        this.setSearchOperator(searchOperator);
        this.setSearch(search);
    }

    public FilterPaging(String searchby, String searchoperator, Object search, boolean defaultCase){
        this.setSearchBy(searchby);
        this.setSearchOperator(searchoperator);
        this.setSearch(search);
        this.setDefaultCase(defaultCase);
    }

    public FilterPaging() {

    }
}
