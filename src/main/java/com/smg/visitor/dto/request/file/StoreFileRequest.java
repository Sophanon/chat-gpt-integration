package com.smg.visitor.dto.request.file;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class StoreFileRequest {


    private MultipartFile file;

}
