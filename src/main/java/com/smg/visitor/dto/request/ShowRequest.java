package com.smg.visitor.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ShowRequest {

    @NotNull
    private int id;

}
