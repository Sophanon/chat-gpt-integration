package com.smg.visitor.dto.request.admin.profile;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UpdateProfilePasswordRequest {

    @NotEmpty
    private String oldPassword;

    @NotEmpty
    private String password;

}
