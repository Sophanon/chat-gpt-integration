package com.smg.visitor.dto.request;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@Data
public class GroupFilterPaging {
    private String filtertype = "AND"; // OR , AND
    private List<FilterPaging> filters = new ArrayList<>();

    public GroupFilterPaging(String filtertype, List<FilterPaging> filters){
        this.setFiltertype(filtertype);
        this.setFilters(filters);
    }
}
