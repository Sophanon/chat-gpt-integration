package com.smg.visitor.dto.request.admin.system_param;

import lombok.Data;

@Data
public class ListSystemParamOptionRequest {

    private boolean production = false;

}
