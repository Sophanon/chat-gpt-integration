package com.smg.visitor.dto.request.admin.system_param;

import com.smg.visitor.dto.request.UpdateRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UpdateSystemParamRequest extends UpdateRequest {

    @NotEmpty
    private String key;

    @NotEmpty
    private String value;
}
