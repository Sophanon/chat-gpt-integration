package com.smg.visitor.dto.request.admin.money_flow;

import lombok.Data;

import java.time.LocalDate;

@Data
public class StoreMoneyFlowRequest {

    private double amount;

    private String description;

    private String type;

    private LocalDate date;

}
