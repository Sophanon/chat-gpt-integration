package com.smg.visitor.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateRequest {

    @NotNull
    private int id;

}
