package com.smg.visitor.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ListRequest implements Serializable {

    private int limit = 10;
    private int page = 0;
    private String sortBy= "id";
    private String sort= "DESC";
    private String filterType = "AND"; // OR , AND
    private List<FilterPaging> filters = new ArrayList<>();

    public int getPage() {
        if(page != 0)
            return page - 1;
        else
            return page;
    }

}
