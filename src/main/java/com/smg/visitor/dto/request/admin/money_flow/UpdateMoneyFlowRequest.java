package com.smg.visitor.dto.request.admin.money_flow;

import com.smg.visitor.dto.request.UpdateRequest;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UpdateMoneyFlowRequest extends UpdateRequest {

    private double amount;

    private String description;

    private String type;

    private LocalDate date;

}
