package com.smg.visitor.dto.request.admin.user;

import com.smg.visitor.dto.request.UpdateRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;


@Data
public class UpdateUserRequest extends UpdateRequest {

    @NotEmpty(message = "Name is required")
    private String userName;
    private String name;

    @NotEmpty(message = "Email is required")
    private String email;


    private String password;

//    @NotEmpty(message = "Enable is required")
    private boolean isEnable;
    private boolean isAdAccount;

    List<StoreRoleRequest> roles;


}
