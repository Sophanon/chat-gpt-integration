package com.smg.visitor.dto.request.admin.profile;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UpdateProfileRequest {

    @NotEmpty
    private String name;

}
