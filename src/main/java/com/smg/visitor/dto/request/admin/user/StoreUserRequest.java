package com.smg.visitor.dto.request.admin.user;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class StoreUserRequest{

    private String name;
    @NotEmpty
    private String userName;

    @NotEmpty(message = "Email is required")
    private String email;

    @NotEmpty(message = "Password is required")
    private String password;

//    @NotEmpty(message = "Enable is required")
    private boolean isEnable;

    List<StoreRoleRequest> roles;

}
