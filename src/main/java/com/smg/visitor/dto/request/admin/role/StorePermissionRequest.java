package com.smg.visitor.dto.request.admin.role;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StorePermissionRequest {

    @NotNull
    private int id;
    private String code;
    private String[] availableActions;
    private String[] permissionAction;

}
