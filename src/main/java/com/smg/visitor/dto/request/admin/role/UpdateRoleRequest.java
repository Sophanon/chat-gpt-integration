package com.smg.visitor.dto.request.admin.role;

import com.smg.visitor.dto.request.UpdateRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class UpdateRoleRequest extends UpdateRequest {

    @NotEmpty
    private String name;

    private String description;

    @NotEmpty
    private List<StorePermissionRequest> abilities;

    private boolean isEnable;

}
