package com.smg.visitor.pdf.export;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.LineSeparator;
import org.json.JSONObject;
import org.seuksa.itextkhmer.KhmerLigaturizer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class StatementPDFExport {

    Document document;
    private String accountHolderName;
    private String accountType;
    private String accountNumber;
    private String accountCurrency;
    private String bankSwiftCode;

    private double openingBalance;
    private double totalMoneyIn;
    private double totalMoneyOut;
    private double endingBalance;
    private PdfPCell logoCell;


    public StatementPDFExport()
    {
        document = new Document(PageSize.A4.rotate(), 5, 5, 20, 30);
        this.accountHolderName = "Sophanon Chhoun";
        this.accountType = "Savings";
        this.accountNumber = "020 484 460 014";
        this.accountCurrency = "USD";
        this.bankSwiftCode = "Prasac";
        this.openingBalance = 100.00;
        this.totalMoneyIn = 100.00;
        this.totalMoneyOut = 100.00;
        this.endingBalance = 100.00;
        this.logoCell = new PdfPCell();
    }

    private void writeTitle() throws DocumentException, IOException {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        // create a cell for the logo
        logoCell.setBorder(Rectangle.NO_BORDER);
        logoCell.setPaddingLeft(0);
        // load the logo image from file
        Image logo = Image.getInstance("src/main/resources/images/logo.png");
        logo.scaleAbsolute(70, 70);
        logoCell.addElement(logo);


        // create a cell for the title and subtitle
        PdfPCell titleCell = new PdfPCell();
        titleCell.setBorder(Rectangle.NO_BORDER);
        titleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        // add the title and subtitle
        Paragraph title = new Paragraph("Prasac MFI", new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD));
        title.setAlignment(Element.ALIGN_RIGHT);
        titleCell.addElement(title);

        Paragraph subtitle = new Paragraph("Account Statement", new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL));
        subtitle.setAlignment(Element.ALIGN_RIGHT);
        titleCell.addElement(subtitle);

        // add the logo and title cells to the table
        table.addCell(logoCell);
        table.addCell(titleCell);
        document.add(table);
    }
    private void addCell(PdfPTable table, String text, int alignment) throws Exception {
        BaseFont baseFont = BaseFont.createFont("fonts/KhmerBattambang/kh-battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(baseFont, 8);

        KhmerLigaturizer d = new KhmerLigaturizer();
        Font englishFont = FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK);
        FontSelector fontSelector = new FontSelector();
        fontSelector.addFont(font);
        fontSelector.addFont(englishFont);
        Phrase phrase = fontSelector.process(d.process(text));

        PdfPCell cell = new PdfPCell();
        cell.setLeading(0, 1.5F);
        cell.setPaddingLeft(5);
        cell.setPaddingRight(5);
        cell.setPaddingBottom(8);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(alignment);
        cell.setPhrase(phrase);
        table.addCell(cell);
    }

    private void addDividerLine() throws Exception {
        // add some margin before the divider
        addEmptyLine(1);

        LineSeparator ls = new LineSeparator();
        ls.setLineColor(new BaseColor(0, 0, 0, 68));
        ls.setLineWidth(1);
        ls.setPercentage(100f);
        document.add(ls);

        // add some margin after the divider
        addEmptyLine(1);
    }
    public void addEmptyLine(int numLines) throws Exception{
        for (int i = 0; i < numLines; i++) {
            document.add(new Paragraph("\n"));
        }
    }

    private void writeDescription() throws Exception {
        // Create the outer table with 2 columns and 2 rows
        PdfPTable outerTable = new PdfPTable(2);
        outerTable.setWidthPercentage(100);

        // Create the nested table for the first cell with 4 rows
        PdfPTable nestedTable1 = new PdfPTable(1);
        nestedTable1.setWidthPercentage(100);
        nestedTable1.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        // Add empty cell to beginning of row
        nestedTable1.addCell(createEmptyCell());

        addSeparateColumn(nestedTable1, "ឈ្នោះ", ":", "Chhoun Sophanon", Element.ALIGN_LEFT, true);
        addSeparateColumn(nestedTable1, "លេខគណនី", ":", "020 484 460 014", Element.ALIGN_LEFT, true);
        addSeparateColumn(nestedTable1, "រូបិយប័ណ្ណ", ":", "ដុល្លារ", Element.ALIGN_LEFT, true);
        addSeparateColumn(nestedTable1, "រយៈពេល", ":", "01/02/2023 - 01/03/2023", Element.ALIGN_LEFT, true);


        // Create the nested table for the second cell with 3 rows
        PdfPTable nestedTable2 = new PdfPTable(1);
        nestedTable2.setWidthPercentage(50);
        nestedTable2.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        // Add empty cell to beginning of row
        nestedTable2.addCell(createEmptyCell());
        addSeparateColumn(nestedTable2, "លុយចូល", ":", "$11.75", Element.ALIGN_RIGHT, false);
        addSeparateColumn(nestedTable2, "លុយចេញ", ":", "$11.25", Element.ALIGN_RIGHT, false);
        addSeparateColumn(nestedTable2, "លុយសល់", ":", "$11.50", Element.ALIGN_RIGHT, false);

        // Add the nested tables to the cells of the outer table
        PdfPCell cell1 = new PdfPCell(nestedTable1);
        cell1.setBorder(Rectangle.NO_BORDER);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cell2 = new PdfPCell(nestedTable2);
        cell2.setBorder(Rectangle.NO_BORDER);
        cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
        outerTable.addCell(cell1);
        outerTable.addCell(cell2);
        outerTable.addCell(cell2);
        // Add the outer table to the document
        document.add(outerTable);
    }

    private PdfPCell createEmptyCell() {
        PdfPCell emptyCell = new PdfPCell();
        emptyCell.setBorder(Rectangle.NO_BORDER);
        return emptyCell;
    }

    private void addCellNoBorder(PdfPTable table, String text, int alignment, int verticalAlignment) throws Exception {
        BaseFont baseFont = BaseFont.createFont("fonts/KhmerBattambang/kh-battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(baseFont, 8);

        KhmerLigaturizer d = new KhmerLigaturizer();
        Font englishFont = FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK);
        FontSelector fontSelector = new FontSelector();
        fontSelector.addFont(font);
        fontSelector.addFont(englishFont);
        Phrase phrase = fontSelector.process(d.process(text));

        PdfPCell cell = new PdfPCell();
        cell.setLeading(0, 1.5F);
        cell.setPaddingLeft(5);
        cell.setPaddingRight(5);
        cell.setPaddingBottom(8);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_LEFT);
        cell.setHorizontalAlignment(alignment);
        cell.setPhrase(phrase);
        table.addCell(cell);
    }

    private void addSeparateColumn(PdfPTable table, String label, String separator, String value, int alignment, boolean leftSide) throws DocumentException, IOException {
        BaseFont baseFont = BaseFont.createFont("fonts/KhmerBattambang/kh-battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(baseFont, 8);

        KhmerLigaturizer d = new KhmerLigaturizer();
        Font englishFont = FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK);
        FontSelector fontSelector = new FontSelector();
        fontSelector.addFont(font);
        fontSelector.addFont(englishFont);
        Phrase phraseLabel = fontSelector.process(d.process(label));
        Phrase phaseSeparator = fontSelector.process(d.process(separator));
        Phrase phaseValue = fontSelector.process(d.process(value));
        PdfPTable nestedTable = new PdfPTable(3);
        nestedTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        nestedTable.setWidthPercentage(50);
        float[] columnWidths = { 0.2f, 0.05f, 0.75f };
        if (leftSide) {
            columnWidths = new float[]{0.2f, 0.05f, 0.75f};
        }else {
            columnWidths = new float[]{0.75f, 0.05f, 0.2f};
        }
        nestedTable.setWidths(columnWidths);
        PdfPCell cellLabel = new PdfPCell();
        cellLabel.setLeading(0, 1.5F);
        cellLabel.setPaddingLeft(5);
        cellLabel.setPaddingRight(5);
        cellLabel.setPaddingBottom(8);
        cellLabel.setVerticalAlignment(Element.ALIGN_LEFT);
        cellLabel.setHorizontalAlignment(alignment);
        cellLabel.setPhrase(phraseLabel);
        cellLabel.setBorder(Rectangle.NO_BORDER);
        PdfPCell cellSeparator = new PdfPCell();
        cellSeparator.setLeading(0, 1.5F);
        cellSeparator.setPaddingLeft(5);
        cellSeparator.setPaddingRight(5);
        cellSeparator.setPaddingBottom(8);
        cellSeparator.setVerticalAlignment(Element.ALIGN_LEFT);
        cellSeparator.setHorizontalAlignment(alignment);
        cellSeparator.setBorder(Rectangle.NO_BORDER);
        cellSeparator.setPhrase(phaseSeparator);
        PdfPCell cellValue = new PdfPCell();
        cellValue.setLeading(0, 1.5F);
        cellValue.setPaddingLeft(5);
        cellValue.setPaddingRight(5);
        cellValue.setPaddingBottom(8);
        cellValue.setVerticalAlignment(Element.ALIGN_LEFT);
        cellValue.setHorizontalAlignment(alignment);
        cellValue.setPhrase(phaseValue);
        cellValue.setBorder(Rectangle.NO_BORDER);
        nestedTable.addCell(cellLabel);
        nestedTable.addCell(cellSeparator);
        nestedTable.addCell(cellValue);
        table.addCell(nestedTable);
    }

    private void writeTable() throws Exception {
        float[] columnWidths;
        int column = 7;
        columnWidths = new float[]{4, 8, 8, 4, 3, 3, 3};
        PdfPTable table = new PdfPTable(column);
        table.setWidths(columnWidths);
        table.setWidthPercentage(100);
        table.setSpacingAfter(40);
        writeTableHeader(table);
        writeTableRow(table);
        document.add(table);
    }


    private void writeTableHeader(PdfPTable table) throws DocumentException, IOException {
        String title = "កាលបរិច្ជេទ,ពិពណ័នា,យោងប្រតិបត្តិការលេខ,ថ្ងៃប្រតិបត្តិការ,ដកប្រាក់,ដាក់ប្រាក់,សមតុល្យ";
        BaseFont baseFont = BaseFont.createFont("fonts/KhmerBattambang/kh-battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(baseFont, 8);

        KhmerLigaturizer d = new KhmerLigaturizer();
        Font englishFont = FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK);
        FontSelector fontSelector = new FontSelector();
        fontSelector.addFont(font);
        fontSelector.addFont(englishFont);
        String[] titles = title.split(",");
        for (String columnTitle : titles) {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.YELLOW);
            header.setBorderWidth(1);
            header.setPadding(5);
            header.setHorizontalAlignment(Element.ALIGN_CENTER);
            header.setVerticalAlignment(Element.ALIGN_MIDDLE);
            Phrase phrase = fontSelector.process(d.process(columnTitle));
            header.setPhrase(phrase);
            table.addCell(header);
        }

        table.completeRow();
    }

    private void writeTableRow(PdfPTable table) throws Exception {
        LocalDate start = LocalDate.of(2000, 1, 1); // set the start date
        LocalDate end = LocalDate.now(); // set the end date to today's date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        UUID uuid = UUID.randomUUID();
        Random random = new Random();
        for (int i = 0; i < random.nextInt(200); i++) {
            long days = ChronoUnit.DAYS.between(start, end); // calculate the number of days between start and end dates
            long randomDays = ThreadLocalRandom.current().nextLong(days + 1); // generate a random number of days

            LocalDate randomDate = start.plusDays(randomDays); // add the random number of days to the start date to get a random date
            addCell(table, randomDate.format(formatter), Element.ALIGN_CENTER);
            String randomString = uuid.toString().replaceAll("-", "");
            addCell(table, randomString, Element.ALIGN_CENTER);
            uuid = UUID.randomUUID();
            randomString = uuid.toString().replaceAll("-", "");
            addCell(table, randomString, Element.ALIGN_CENTER);
            randomDays = ThreadLocalRandom.current().nextLong(days + 1);
            randomDate = start.plusDays(randomDays);
            addCell(table, randomDate.format(formatter), Element.ALIGN_CENTER);
            boolean randomBoolean = random.nextBoolean();
            if (randomBoolean) {
                addCell(table, String.format("%.2f", random.nextDouble()), Element.ALIGN_CENTER);
                addCell(table, "", Element.ALIGN_CENTER);
            }else {
                addCell(table, "", Element.ALIGN_CENTER);
                addCell(table, String.format("%.2f", random.nextDouble()), Element.ALIGN_CENTER);
            }
            addCell(table, String.format("%.2f", random.nextDouble()), Element.ALIGN_CENTER);
        }
    }



    public ByteArrayInputStream export() throws Exception {


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, stream);
        PdfPageNumber event = new PdfPageNumber();
        writer.setPageEvent(event);

        document.open();
        writeTitle();
        addDividerLine();
        writeDescription();
        writeTable();
        document.close();

        return new ByteArrayInputStream(stream.toByteArray());
    }


    public static class PdfPageNumber extends PdfPageEventHelper {
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            Font font = FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK);

            try {
                Rectangle pageSize = document.getPageSize();
                ColumnText.showTextAligned(writer.getDirectContent(),
                        Element.ALIGN_RIGHT,
                        new Phrase(String.format("Page %s", writer.getCurrentPageNumber()), font),
                        pageSize.getRight(20),
                        pageSize.getBottom(15), 0);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
