package com.smg.visitor.pdf.export;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.seuksa.itextkhmer.KhmerLigaturizer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

public class TDPDFExport {

    Document document;
    private PdfPCell logoCell;
    private DateTimeFormatter formatter;

    public TDPDFExport()
    {
        document = new Document(PageSize.A4, 35, 20, 10, 0);
        this.logoCell = new PdfPCell();
        formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    private void writeTitle() throws DocumentException, IOException {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        // create a cell for the logo
        logoCell.setBorder(Rectangle.NO_BORDER);
        logoCell.setPaddingLeft(-20);
        logoCell.setPaddingTop(0);
        logoCell.setPaddingBottom(0);
        logoCell.setPaddingRight(5);
        logoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

        // load the logo image from file
        Image logo = Image.getInstance("src/main/resources/images/logo.png");
        logo.scaleAbsolute(135, 135);
        logoCell.addElement(logo);
        table.addCell(logoCell); // Add the logoCell to the table
        PdfPCell titleCell = new PdfPCell(getPhrase("វិញ្ញាបនប័ត្រនៃការដាក់ប្រាក់តាមកាលកំណត់", 14, true));
        titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        titleCell.setBorder(Rectangle.NO_BORDER); // remove the border
        table.addCell(titleCell); // add the titleCell to the table

        document.add(table); // Add the table to the document
    }

    private void addSeparateColumn(PdfPTable table, String label, String value, int alignment) throws DocumentException, IOException {
        PdfPTable nestedTable = new PdfPTable(2);
        nestedTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        nestedTable.setWidthPercentage(50);
        float[] columnWidths = { 0.4f, 0.6f};
        nestedTable.setWidths(columnWidths);
        PdfPCell cellLabel = new PdfPCell();
        cellLabel.setLeading(0, 1.5F);
        cellLabel.setPaddingLeft(5);
        cellLabel.setPaddingRight(5);
        cellLabel.setPaddingBottom(8);
        cellLabel.setVerticalAlignment(Element.ALIGN_LEFT);
        cellLabel.setHorizontalAlignment(alignment);
        cellLabel.setPhrase(getPhrase(label, 12, true));
        cellLabel.setBorder(Rectangle.NO_BORDER);
        PdfPCell cellValue = new PdfPCell();
        cellValue.setLeading(0, 1.5F);
        cellValue.setPaddingLeft(5);
        cellValue.setPaddingRight(5);
        cellValue.setPaddingBottom(8);
        cellValue.setVerticalAlignment(Element.ALIGN_LEFT);
        cellValue.setHorizontalAlignment(alignment);
        cellValue.setBorder(Rectangle.NO_BORDER);
        cellValue.setPhrase(getPhrase(value, 12, false));
        nestedTable.addCell(cellLabel);
        nestedTable.addCell(cellValue);
        table.addCell(nestedTable);
    }

    private void writeDate() throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);

        float[] columnWidths = { 0.88f, 0.12f};
        table.setWidths(columnWidths);

        // add the "Date" label with bold font
        PdfPCell titleCell = new PdfPCell(getPhrase("កាលបរិច្ឆេទ", 8, true));
        titleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        titleCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(titleCell);

        // add the date value with non-bold font
        PdfPCell dateCell = new PdfPCell(getPhrase(": " + LocalDate.now().format(formatter), 8, false));
        dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        dateCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(dateCell);

        document.add(table); // Add the table to the document
    }

    private void addRow(String label1, String value1, String label2, String value2) throws Exception {
        document.add(new Paragraph("\n"));
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        float[] columnWidths = { 1f, 1f };
        table.setWidths(columnWidths);
        PdfPCell cell1 = new PdfPCell(addNestedTable(label1, value1));
        PdfPCell cell2 = new PdfPCell(addNestedTable(label2, value2));
        cell1.setBorder(Rectangle.NO_BORDER);
        cell2.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell1);
        table.addCell(cell2);
        document.add(table);
    }

    private void addRowTwoColumn(String label, String value) throws Exception {
        document.add(new Paragraph("\n"));
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);

        float[] columnWidths = { 0.2f, 0.8f};
        table.setWidths(columnWidths);

        // add the "Date" label with bold font
        PdfPCell titleCell = new PdfPCell(getPhrase(label, 8, true));
        titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        titleCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(titleCell);

        // add the date value with non-bold font
        PdfPCell dateCell = new PdfPCell(getPhrase(value, 8, false));
        dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        dateCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(dateCell);

        document.add(table); // Add the table to the document
    }

    private PdfPTable addNestedTable(String label, String value) throws Exception
    {
        PdfPCell cell1 = new PdfPCell(getPhrase(label, 8, true));
        cell1.setBorder(Rectangle.NO_BORDER);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cell2 = new PdfPCell(getPhrase(value, 8, false));
        cell2.setBorder(Rectangle.NO_BORDER);
        PdfPTable nestedTable = new PdfPTable(2);
        nestedTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        nestedTable.setWidthPercentage(100);
        float[] nestedColumnWidths1 = { 0.4f, 0.6f  };
        nestedTable.setWidths(nestedColumnWidths1);
        nestedTable.addCell(cell1);
        nestedTable.addCell(cell2);
        return nestedTable;
    }


    public Phrase getPhrase(String text,int fontSize, boolean bold) throws DocumentException, IOException {
        BaseFont baseFont = BaseFont.createFont("fonts/KhmerBattambang/kh-battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font;
        if (bold) {
            font = new Font(baseFont, fontSize, Font.BOLD);
        }else {
            font = new Font(baseFont, fontSize);
        }

        KhmerLigaturizer d = new KhmerLigaturizer();
        Font englishFont = FontFactory.getFont(FontFactory.HELVETICA, fontSize, BaseColor.BLACK);
        FontSelector fontSelector = new FontSelector();
        fontSelector.addFont(font);
        fontSelector.addFont(englishFont);
        return fontSelector.process(d.process(text));
    }

    public ByteArrayInputStream export() throws Exception {


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, stream);
        PdfPageNumber event = new PdfPageNumber();
        writer.setPageEvent(event);

        document.open();
        writeTitle();
        for (int i = 0; i < 1; i++) {
            document.add(new Paragraph("\n"));
        }
        writeDate();
        addRow("ឈ្មោះគណនី", ": Chhoun Sophanon", "ប្រភេទគណនី", ": Maturity Growth Account");
        addRow("លេខគណនី", ": 020 484 460 116", "ប្រភពគណនី លេខ", ": 020 484 460 014");
        addRowTwoColumn("អាស័យដ្ឋាន", ": #159A,ST.156Z, PHUM2, TUEK LA AK TI PIR, TUOL KOUK, PHNOM PENH");
        addRowTwoColumn("នាយកនៅក្នុងរូបភាព", ": 90 USD");
        addRow("រយៈពេល", ": 12 Months (365 DAYS)", "ជម្រើសវិលជុំ", ": CLOSE ON MATURITY");
        addRow("អត្រាការប្រាក់", ": 7.25%", "អត្រាពន្ធ", ": 6.00%");
        addRow("ការបរិច្ឆេទមានប្រសិទ្ធភាព", ": 12-05-2022", "កាលបរិច្ឆេទផុតកំណត់", ": 12-05-2023");
        addRow("ការប្រាក់សុទ្ធ (ក្រោយពន្ធ)", ": 136.30 USD", "ចំនួនសរុប (សុទ្ធ)", ": 100 USD");
        document.close();

        return new ByteArrayInputStream(stream.toByteArray());
    }


    public static class PdfPageNumber extends PdfPageEventHelper {
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                // Add page number to bottom right corner
                Image image = Image.getInstance("src/main/resources/images/footer.png");
                image.scaleAbsolute(document.getPageSize().getWidth() - 20, 40);
                // Get the page size and position the image
                Rectangle pageSize = document.getPageSize();
                float xPos = pageSize.getRight(20) - image.getScaledWidth();
                float yPos = pageSize.getBottom(15);

                // Add the image to the page
                PdfContentByte canvas = writer.getDirectContent();
                canvas.addImage(image, image.getScaledWidth(), 0, 0, image.getScaledHeight(), xPos, yPos);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
