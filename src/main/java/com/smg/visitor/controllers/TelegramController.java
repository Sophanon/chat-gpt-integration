package com.smg.visitor.controllers;

import com.smg.visitor.dto.request.TelegramMessageRequest;
import com.smg.visitor.services.TelegramService;
import com.smg.visitor.util.TelegramUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController()
@RequestMapping("/api/telegram")
public class TelegramController {

    @Autowired
    private TelegramService telegramService;

    @PostMapping("/webhook")
    public ResponseEntity<Object> webhookTelegram(@RequestBody Map<String, Object> request) throws Exception {
        telegramService.webhook(request);
        return ResponseEntity.ok("success");
    }


}
