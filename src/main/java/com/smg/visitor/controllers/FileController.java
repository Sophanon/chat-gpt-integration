package com.smg.visitor.controllers;

import com.smg.visitor.dto.request.file.FilePagingRequest;
import com.smg.visitor.dto.request.file.StoreFileRequest;
import com.smg.visitor.dto.response.ResponseData;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.services.FileService;
import com.smg.visitor.services.S3Service;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/file")
public class FileController {

    @Autowired
    private S3Service s3Service;

    @Autowired
    private FileService fileService;

    @PostMapping(headers = "action=list")
    @Secured({RoleKeyEnum.ROLE_ADMIN})
    public ResponseEntity<?> index(@RequestBody FilePagingRequest request) throws Exception
    {
        return ResponseData.data(200, fileService.listForAdmin(request), null);
    }

    @PostMapping(headers = "action=upload-file")
    @Secured({RoleKeyEnum.ROLE_ADMIN})
    public ResponseEntity<?> store(StoreFileRequest request) throws Exception
    {
        return ResponseData.data(200, s3Service.uploadImage(request.getFile()), ResponseMessage.success);
    }

}
