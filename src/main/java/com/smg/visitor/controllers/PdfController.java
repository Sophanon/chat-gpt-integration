package com.smg.visitor.controllers;

import com.smg.visitor.pdf.export.StatementPDFExport;
import com.smg.visitor.pdf.export.TDPDFExport;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.*;


@RestController()
@RequestMapping("/api/pdf")
public class PdfController {

    @GetMapping("/export")
    public ResponseEntity<ByteArrayResource> downloadPDF() throws Exception {
        try {
            StatementPDFExport pdf = new StatementPDFExport();
            ByteArrayInputStream inputStream = pdf.export();
            byte[] data = inputStream.readAllBytes();
            ByteArrayResource resource = new ByteArrayResource(data);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment; filename=output.pdf");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(data.length)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/export/td")
    public ResponseEntity<ByteArrayResource> downloadTDPdf() throws Exception {
        try {
            TDPDFExport pdf = new TDPDFExport();
            ByteArrayInputStream inputStream = pdf.export();
            byte[] data = inputStream.readAllBytes();
            ByteArrayResource resource = new ByteArrayResource(data);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment; filename=output.pdf");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(data.length)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
