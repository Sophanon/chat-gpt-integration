package com.smg.visitor.controllers.admin;

import com.smg.visitor.dto.request.ShowRequest;
import com.smg.visitor.dto.request.StatusRequest;
import com.smg.visitor.dto.request.admin.role.RolePagingRequest;
import com.smg.visitor.dto.request.admin.role.StoreRoleRequest;
import com.smg.visitor.dto.request.admin.role.UpdateRoleRequest;
import com.smg.visitor.dto.response.ResponseData;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.services.admin.RoleService;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/admin/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=list")
    public ResponseEntity<?> index(@RequestBody RolePagingRequest request) throws Exception
    {
        return ResponseData.data(200, roleService.listForAdmin(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=list-permission")
    public ResponseEntity<?> listPermission() throws Exception
    {
        return ResponseData.data(200, roleService.permissions(), null);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_CREATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=store")
    public ResponseEntity<?> store(@Valid @RequestBody StoreRoleRequest request) throws Exception
    {
        return ResponseData.data(200, roleService.store(request), ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_UPDATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=update")
    public ResponseEntity<?> update(@Valid @RequestBody UpdateRoleRequest request) throws Exception
    {
        return ResponseData.data(200, roleService.update(request), ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=show")
    public ResponseEntity<?> show(@Valid @RequestBody ShowRequest request) throws Exception
    {
        return ResponseData.data(200, roleService.show(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_DELETE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=delete")
    public ResponseEntity<?> delete(@Valid @RequestBody ShowRequest request) throws Exception
    {
        roleService.delete(request);
        return ResponseData.data(200, null, ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=option")
    public ResponseEntity<?> option(@RequestBody RolePagingRequest request) throws Exception
    {
        return ResponseData.data(200, roleService.options(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=update-status")
    public ResponseEntity<?> updateStatus(@Valid @RequestBody StatusRequest request) throws Exception
    {
        return ResponseData.data(200, roleService.updateStatus(request), ResponseMessage.success);
    }

}
