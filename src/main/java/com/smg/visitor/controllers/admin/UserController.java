package com.smg.visitor.controllers.admin;

import com.smg.visitor.dto.request.ShowRequest;
import com.smg.visitor.dto.request.StatusRequest;
import com.smg.visitor.dto.request.admin.user.StoreUserRequest;
import com.smg.visitor.dto.request.admin.user.UpdateUserRequest;
import com.smg.visitor.dto.request.admin.user.UserPagingRequest;
import com.smg.visitor.dto.response.ResponseData;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.services.admin.UserService;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/admin/users")
public class UserController {

    @Autowired
    UserService userService;

    @Secured({RoleKeyEnum.ROLE_ADMINISTRATOR_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=list")
    public ResponseEntity<?> index(@RequestBody UserPagingRequest request) throws Exception
    {
        return ResponseData.data(200, userService.listForUser(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ADMINISTRATOR_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=show")
    public ResponseEntity<?> show(@Valid @RequestBody ShowRequest request) throws Exception
    {
        return ResponseData.data(200, userService.show(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ADMINISTRATOR_CREATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=store")
    public ResponseEntity<?> store(@Valid @RequestBody StoreUserRequest request) throws Exception
    {
        return ResponseData.data(200, userService.store(request), ResponseMessage.success);
    }


    @Secured({RoleKeyEnum.ROLE_ADMINISTRATOR_UPDATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=update")
    public ResponseEntity<?> update(@Valid @RequestBody UpdateUserRequest request) throws Exception
    {
        return ResponseData.data(200, userService.update(request), ResponseMessage.UPDATED_SUCCESS);
    }


    @Secured({RoleKeyEnum.ROLE_ADMINISTRATOR_DELETE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=delete")
    public ResponseEntity<?> delete(@Valid @RequestBody ShowRequest request) throws Exception
    {
        userService.delete(request);
        return ResponseData.data(200, null, ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=update-status")
    public ResponseEntity<?> updateStatus(@Valid @RequestBody StatusRequest request) throws Exception
    {
        return ResponseData.data(200, userService.updateStatus(request), ResponseMessage.success);
    }


}
