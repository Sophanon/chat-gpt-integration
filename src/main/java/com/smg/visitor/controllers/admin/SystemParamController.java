package com.smg.visitor.controllers.admin;

import com.smg.visitor.dto.request.admin.system_param.SystemParamPagingRequest;
import com.smg.visitor.dto.request.admin.system_param.UpdateSystemParamRequest;
import com.smg.visitor.dto.response.ResponseData;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.services.admin.SystemParamService;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/admin/system_param")
public class SystemParamController {

    @Autowired
    private SystemParamService systemParamService;

    @Secured({RoleKeyEnum.ROLE_SYSTEM_PARAM_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=list")
    public ResponseEntity<?> index(@RequestBody SystemParamPagingRequest request) throws Exception
    {
        return ResponseData.data(200, systemParamService.index(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_SYSTEM_PARAM_UPDATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=update")
    public ResponseEntity<?> update(@Valid @RequestBody UpdateSystemParamRequest request) throws Exception
    {
        systemParamService.update(request);
        return ResponseData.data(200, null, ResponseMessage.success);
    }
}
