package com.smg.visitor.controllers.admin;

import com.smg.visitor.dto.request.admin.auth.AdminAuthLoginRequest;
import com.smg.visitor.dto.request.admin.auth.AdminAuthRefreshRequest;
import com.smg.visitor.dto.request.admin.profile.UpdateProfilePasswordRequest;
import com.smg.visitor.dto.request.admin.profile.UpdateProfileRequest;
import com.smg.visitor.dto.response.ResponseData;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.services.admin.AdminAuthService;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/admin/auth")
public class AdminAuthController {

    @Autowired
    private AdminAuthService authService;

    @PostMapping(headers = "action=login")
    public ResponseEntity<?> login(@Valid @RequestBody AdminAuthLoginRequest request) throws Exception
    {
        return ResponseData.data(200, authService.login(request), ResponseMessage.LOGIN_SUCCESS);
    }

    @PostMapping(headers = "action=refresh-token")
    public ResponseEntity<?> refresh(@RequestBody AdminAuthRefreshRequest request) throws Exception
    {
        return ResponseData.data(200, authService.refreshToken(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(value = "", headers = "action=get-profile")
    public ResponseEntity<?> getProfile() throws Exception
    {
        return ResponseData.data(200, authService.getProfile(), null);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(value = "", headers = "action=update-profile")
    public ResponseEntity<?> updateProfile(@Valid @RequestBody UpdateProfileRequest request) throws Exception
    {
        return ResponseData.data(200, authService.updateProfile(request), ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(value = "", headers = "action=update-password")
    public ResponseEntity<?> updatePassword(@Valid @RequestBody UpdateProfilePasswordRequest request) throws Exception
    {
        return ResponseData.data(200, authService.updatePassword(request), ResponseMessage.success);
    }

    @PostMapping(headers = "action=logout")
    public ResponseEntity<?> logout(@RequestBody AdminAuthRefreshRequest request) throws Exception
    {
        authService.logout(request);
        return ResponseData.data(200, null, null);
    }
}
