package com.smg.visitor.controllers.admin;

import com.smg.visitor.dto.request.ListRequest;
import com.smg.visitor.dto.request.ShowRequest;
import com.smg.visitor.dto.request.admin.money_flow.StoreMoneyFlowRequest;
import com.smg.visitor.dto.request.admin.money_flow.UpdateMoneyFlowRequest;
import com.smg.visitor.dto.response.ResponseData;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.services.admin.MoneyFlowService;
import com.smg.visitor.util.RoleKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/admin/money_flow")
public class MoneyFlowController {

    @Autowired
    private MoneyFlowService service;

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=list")
    public ResponseEntity<?> index(@RequestBody ListRequest request) throws Exception
    {
        return ResponseData.data(200, service.list(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_CREATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=store")
    public ResponseEntity<?> store(@Valid @RequestBody StoreMoneyFlowRequest request) throws Exception
    {
        return ResponseData.data(200, service.store(request), ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_UPDATE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=update")
    public ResponseEntity<?> update(@Valid @RequestBody UpdateMoneyFlowRequest request) throws Exception
    {
        return ResponseData.data(200, service.update(request), ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_READ, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=show")
    public ResponseEntity<?> show(@Valid @RequestBody ShowRequest request) throws Exception
    {
        return ResponseData.data(200, service.show(request), null);
    }

    @Secured({RoleKeyEnum.ROLE_ROLE_PERMISSION_DELETE, RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=delete")
    public ResponseEntity<?> delete(@Valid @RequestBody ShowRequest request) throws Exception
    {
        service.delete(request);
        return ResponseData.data(200, null, ResponseMessage.success);
    }

    @Secured({RoleKeyEnum.ROLE_ADMIN})
    @PostMapping(headers = "action=types")
    public ResponseEntity<?> types() throws Exception
    {
        return ResponseData.data(200, service.types(), null);
    }
}
