package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface PgRoleRepository extends JpaRepository<PgRole, Integer>, PagingAndSortingRepository<PgRole, Integer>, JpaSpecificationExecutor<PgRole> {

    Optional<PgRole> findByName(String name);

    Optional<PgRole> findTopByName(String name);

    List<PgRole> findAllByIdIn(Collection<Integer> id);
    Optional<PgRole> findByNameAndIdNot(String name, Integer excludeId);

    @Modifying
    @Transactional
    @Query("update PgRole pgrole set pgrole.deletedAt = ?2, pgrole.deletedBy=?3 where pgrole.id=?1")
    void softDelete(int id, LocalDateTime deletedAt, int deletedBy);

}
