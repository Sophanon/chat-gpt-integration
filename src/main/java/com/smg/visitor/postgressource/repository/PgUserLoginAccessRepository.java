package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgUserLoginAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface PgUserLoginAccessRepository extends JpaRepository<PgUserLoginAccess, Integer>, JpaSpecificationExecutor<PgUserLoginAccess> {
    Optional<PgUserLoginAccess> findTopByAccessToken(String accessToken);


    Optional<PgUserLoginAccess> findTopByAccessTokenAndRevokeAndAccessTokenExpiredAtAfter(String accessToken, boolean revoke,LocalDateTime expiredAt);

    Optional<PgUserLoginAccess> findTopByRefreshTokenAndRevokeAndRefreshTokenExpiredAtAfter(String refreshToken, boolean revoke, LocalDateTime expiredAt);

    Optional<PgUserLoginAccess> findTopByRefreshToken(String refreshToken);

    @Transactional
    @Modifying
    @Query("update PgUserLoginAccess pguser set pguser.revoke = true where pguser.userId = ?1 ")
    void updateRevokeToFalseByUserId(int userId);

}
