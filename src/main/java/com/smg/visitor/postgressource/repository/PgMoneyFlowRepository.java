package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgMoneyFlow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Repository
public interface PgMoneyFlowRepository extends JpaRepository<PgMoneyFlow, Integer>, PagingAndSortingRepository<PgMoneyFlow, Integer>, JpaSpecificationExecutor<PgMoneyFlow> {



    @Modifying
    @Transactional
    @Query("update PgMoneyFlow pg set pg.deletedAt = ?2, pg.deletedBy=?3 where pg.id=?1")
    void softDelete(int id, LocalDateTime deletedAt, int deletedBy);
}