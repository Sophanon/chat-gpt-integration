package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgMedia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PgMediaRepository extends JpaRepository<PgMedia, Integer>, PagingAndSortingRepository<PgMedia, Integer>, JpaSpecificationExecutor<PgMedia> {

}
