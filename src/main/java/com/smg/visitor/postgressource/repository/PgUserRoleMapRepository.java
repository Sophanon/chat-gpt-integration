package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgUserRoleMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface PgUserRoleMapRepository extends JpaRepository<PgUserRoleMap, Integer>, JpaSpecificationExecutor<PgUserRoleMap> {
    Optional<PgUserRoleMap> findTopByUserIdAndRoleId(int userId, int roleId);

    @Transactional
    void deleteByUserId(int userId);

    @Query("SELECT r.roleId from PgUserRoleMap r where r.userId = ?1")
    Integer[] getRoleIdByUserId(int userId);

}
