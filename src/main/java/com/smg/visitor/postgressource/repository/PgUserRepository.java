package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface PgUserRepository extends JpaRepository<PgUser, Integer>, PagingAndSortingRepository<PgUser, Integer>, JpaSpecificationExecutor<PgUser>{

    Optional<PgUser> findTopByEmail(String email);

    Optional<PgUser> findTopByUserName(String userName);
    Optional<PgUser> findTopByUserNameAndIdNot(String userName,int excludeId);

    Page<PgUser> findAll(Specification<PgUser> filter, Pageable page);
    Optional<PgUser> findTopByEmailOrUserName(String email, String name);

    @Modifying
    @Transactional
    @Query("update PgUser pguser set pguser.deletedAt = ?2, pguser.deletedBy=?3 where pguser.id=?1")
    void softDelete(int id, LocalDateTime deletedAt, int deletedBy);
}
