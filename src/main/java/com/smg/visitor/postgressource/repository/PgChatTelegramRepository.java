package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgChatTelegram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PgChatTelegramRepository extends JpaRepository<PgChatTelegram, Integer>, JpaSpecificationExecutor<PgChatTelegram> {

    List<PgChatTelegram> findAllByTelegramChatId(String telegramChatId);

}
