package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PgPermissionRepository extends JpaRepository<PgPermission, Integer> {

    Optional<PgPermission> findByCode(String code);

}
