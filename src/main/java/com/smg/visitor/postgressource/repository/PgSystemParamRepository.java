package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgSystemParam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PgSystemParamRepository extends JpaRepository<PgSystemParam, Integer>, PagingAndSortingRepository<PgSystemParam, Integer>, JpaSpecificationExecutor<PgSystemParam> {

    Optional<PgSystemParam> findTopByKey(String key);

}
