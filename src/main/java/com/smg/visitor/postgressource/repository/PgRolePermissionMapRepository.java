package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.PgRolePermissionMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface PgRolePermissionMapRepository extends JpaRepository<PgRolePermissionMap, Integer>, JpaSpecificationExecutor<PgRolePermissionMap> {


    List<PgRolePermissionMap> findAllByRoleId(int roleId);
    Optional<PgRolePermissionMap> findByRoleIdAndPermissionId(int roleId, int permissionId);
    List<PgRolePermissionMap> findAllByRoleIdIn(Integer[] roleId);
    Optional<PgRolePermissionMap> getTopByPermissionIdAndRoleId(Integer permissionId, Integer roleId);


    @Transactional
    void deleteAllByRoleId(Integer id);
}
