package com.smg.visitor.postgressource.repository;

import com.smg.visitor.postgressource.entity.Shedlock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShedlockRepository extends JpaRepository<Shedlock, Integer> {
    Optional<Shedlock> getTopByName(String name);

}