package com.smg.visitor.postgressource.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "pg_role_admins", indexes = {@Index(columnList = "id"),
        @Index(columnList = "name")})
public class PgRole extends BaseByEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "is_enable")
    private boolean isEnable;

}
