package com.smg.visitor.postgressource.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseByEntity extends BaseEntity{

    @Column(name = "created_by", nullable = true)
    @CreatedBy
    private Integer createdBy;

    @Column(name = "updated_by", nullable = true)
    @LastModifiedBy
    private Integer updatedBy;

    @Column(name = "deleted_by", nullable = true)
    private Integer deletedBy;

}
