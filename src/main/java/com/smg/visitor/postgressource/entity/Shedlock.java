package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "shedlock")
public class Shedlock {

    @Id
    private String name;
    private String lock_until;
    private String locked_at;
    private String locked_by;
}
