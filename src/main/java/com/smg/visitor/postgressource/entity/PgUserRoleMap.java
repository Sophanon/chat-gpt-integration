package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pg_user_role_maps", indexes = {@Index(columnList = "role_id"),
        @Index(columnList = "user_id")})
public class PgUserRoleMap extends BaseByEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "role_id", nullable = false)
    private int roleId;

    @Column(name = "user_id", nullable = false)
    private int userId;

}
