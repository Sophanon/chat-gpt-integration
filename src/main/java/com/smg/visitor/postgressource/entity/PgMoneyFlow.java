package com.smg.visitor.postgressource.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "pg_money_flow", indexes = {@Index(columnList = "id"),
        @Index(columnList = "description"), @Index(columnList = "amount"), @Index(columnList = "type")})
public class PgMoneyFlow extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "amount")
    private double amount;

    @Column(name = "type")
    private String type;


    @Column(name = "date")
    private LocalDate date;
}