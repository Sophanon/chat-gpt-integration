package com.smg.visitor.postgressource.entity;

import com.smg.visitor.util.CoreBase;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "pg_users", indexes = {@Index(columnList = "name"),
        @Index(columnList = "email"), @Index(columnList = "user_name")})
public class PgUser extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "user_name", nullable = false, unique = true)
    private String userName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "is_ad_account", columnDefinition="boolean default true")
    private boolean isAdAccount;
    @Column(name = "is_enable")
    private boolean isEnable;

    public PgUser() {}


    public void setPassword(String password) {
        this.password = CoreBase.encryptPassword(password);
    }


}
