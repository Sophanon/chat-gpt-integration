package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pg_system_params", indexes = {@Index(columnList = "id"),
        @Index(columnList = "key")})
public class PgSystemParam extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "value", nullable = false)
    private String value;

    public PgSystemParam() {}

    public PgSystemParam(String key, String value) {
        this.key = key;
        this.value = value;
    }

}
