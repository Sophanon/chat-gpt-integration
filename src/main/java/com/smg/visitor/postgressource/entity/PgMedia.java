package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "pg_medias", indexes = {
    @Index(columnList = "id"), @Index(columnList = "name"), @Index(columnList = "path")
})
public class PgMedia extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "path", nullable = false)
    private String path;

    @Column(name = "type", columnDefinition = "VARCHAR(255) DEFAULT 'image'")
    private String type;

    @Column(name = "extension", columnDefinition = "VARCHAR(255) DEFAULT 'png'")
    private String extension;

}
