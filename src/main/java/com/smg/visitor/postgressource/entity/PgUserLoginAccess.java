package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "pg_user_login_accesses", indexes = {@Index(columnList = "access_token"),
        @Index(columnList = "refresh_token"), @Index(columnList = "access_token_expired_at"), @Index(columnList = "refresh_token_expired_at")})
public class PgUserLoginAccess extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "access_token", nullable = false)
    private String accessToken;

    @Column(name = "refresh_token", nullable = false)
    private String refreshToken;

    @Column(name = "access_token_expired_at", nullable = false)
    private LocalDateTime accessTokenExpiredAt;

    @Column(name = "refresh_token_expired_at", nullable = false)
    private LocalDateTime refreshTokenExpiredAt;

    @Column(name = "revoke")
    private boolean revoke;

}
