package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pg_role_permission_maps", indexes = {@Index(columnList = "role_id")})
public class PgRolePermissionMap extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "role_id", nullable = false)
    private int roleId;

    @Column(name = "permission_id", nullable = false)
    private int permissionId;

    @Column(name = "actions", nullable = false)
    private String actions;



}
