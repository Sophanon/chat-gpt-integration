package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "pg_chat_telegram", indexes = {
        @Index(columnList = "id"), @Index(columnList = "telegram_chat_id")
})
public class PgChatTelegram extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "role")
    private String role;

    @Column(name = "telegram_chat_id")
    private String telegramChatId;

    @Column(name = "is_money")
    private boolean isMoney=false;

}
