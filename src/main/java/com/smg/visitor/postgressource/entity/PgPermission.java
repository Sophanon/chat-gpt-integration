package com.smg.visitor.postgressource.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "pg_permissions")
public class PgPermission extends BaseByEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "module", nullable = false)
    private String module;

    @Column(name = "sequence_order", columnDefinition = "int default 1")
    private int sequenceOrder;

    @Column(name = "actions", nullable = false)
    private String actions;


}
