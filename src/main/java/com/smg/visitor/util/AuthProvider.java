package com.smg.visitor.util;

import com.smg.visitor.dto.request.admin.auth.AuthProfileRequest;
import com.smg.visitor.postgressource.entity.PgUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthProvider {

    @Autowired
    EncryptDecryptProvider encryptDecryptProvider;

    private boolean login = false;

    public AuthProfileRequest getAuth() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            return (AuthProfileRequest) auth.getPrincipal();
        }
        return null;
    }

    public PgUser getAuthUser(){
        AuthProfileRequest authProfile = this.getAuth();
        if(authProfile != null){
            PgUser user = CoreBase.getUser(authProfile.getId());
            return user;
        }
        return null;
    }


    public void setErrorMessage(String message)
    {
        this.getAuth().setError(message);
    }
}
