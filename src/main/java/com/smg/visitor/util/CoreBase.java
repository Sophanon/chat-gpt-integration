package com.smg.visitor.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.smg.visitor.dto.response.admin.auth.UserProfileResponse;
import com.smg.visitor.dto.response.admin.role.ListRoleOptionResponse;
import com.smg.visitor.dto.response.admin.role.ShowPermissionResponse;
import com.smg.visitor.postgressource.entity.PgPermission;
import com.smg.visitor.postgressource.entity.PgRole;
import com.smg.visitor.postgressource.entity.PgRolePermissionMap;
import com.smg.visitor.postgressource.entity.PgUser;
import com.smg.visitor.postgressource.repository.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CoreBase {

    public static AuthProvider authProvider;
    private static PgUserRepository pgUserRepository;
    private static PgUserRoleMapRepository pgUserRoleMapRepository;
    private static PgRoleRepository pgRoleRepository;
    private static PgRolePermissionMapRepository pgRolePermissionMapRepository;

    private static PgPermissionRepository pgPermissionRepository;

    private static PgSystemParamRepository pgSystemParamRepository;

    private static EncryptDecryptProvider encryptDecryptProvider;

    private static PgMediaRepository pgMediaRepository;

    private static PasswordEncoder passwordEncoder;

    @Autowired
    public CoreBase(PgUserRepository pgUserRepository, PgUserRoleMapRepository pgUserRoleMapRepository, PgRoleRepository pgRoleRepository,
                    PgRolePermissionMapRepository pgRolePermissionMapRepository, PgPermissionRepository pgPermissionRepository, AuthProvider authProvider,
                    PgSystemParamRepository pgSystemParamRepository, EncryptDecryptProvider encryptDecryptProvider, PgMediaRepository pgMediaRepository, PasswordEncoder passwordEncoder
    ) {
        this.pgUserRepository = pgUserRepository;
        this.pgUserRoleMapRepository = pgUserRoleMapRepository;
        this.pgRoleRepository = pgRoleRepository;
        this.pgRolePermissionMapRepository = pgRolePermissionMapRepository;
        this.pgPermissionRepository = pgPermissionRepository;
        this.authProvider = authProvider;
        this.pgSystemParamRepository = pgSystemParamRepository;
        this.encryptDecryptProvider = encryptDecryptProvider;
        this.passwordEncoder = passwordEncoder;
        this.pgMediaRepository = pgMediaRepository;
    }

    public static UserProfileResponse getUserById(int id) throws Exception {
        return new UserProfileResponse(Objects.requireNonNull(pgUserRepository.findById(id).orElse(null)));
    }

    public static String[] getAvailableActions(String actions) {
        actions = actions.replace("[", "").replace("]", "");
        return actions.split("\\|");
    }

    public static PgUser getUser(int userId)
    {
        Optional<PgUser> optionalUser = pgUserRepository.findById(userId);
        return optionalUser.orElse(null);
    }

    public static List<ListRoleOptionResponse> getRoleByUserId(int userId)
    {
        Integer[] rolesIds = pgUserRoleMapRepository.getRoleIdByUserId(userId);
        List<PgRole> pgRoles = pgRoleRepository.findAllByIdIn(List.of(rolesIds));
        return (List<ListRoleOptionResponse>) ObjectConverter.converArraytToArrayObject(pgRoles, new TypeReference<List<ListRoleOptionResponse>>() {
        });
    }

    public static Object getUserAbilities(int userId) {
        Integer[] roleIds = pgUserRoleMapRepository.getRoleIdByUserId(userId);
        List<PgRolePermissionMap> rolePermissionMaps = pgRolePermissionMapRepository.findAllByRoleIdIn(roleIds);
        List<HashMap<String, Object>> list = new ArrayList<>();
        rolePermissionMaps.forEach(rolePermissionMap -> {
            PgPermission permission = pgPermissionRepository.findById(rolePermissionMap.getPermissionId()).orElse(null);
            String[] permissionActions = permission.getActions().split("\\|");
            String[] actions = rolePermissionMap.getActions().split("\\|");
            if (permissionActions.length == actions.length) {
                HashMap<String, Object> row = new HashMap<>();
                row.put("subject", permission.getCode());
                row.put("action", "manage");
                list.add(row);
            }else {
                for (String action : actions) {
                    HashMap<String, Object> row = new HashMap<>();
                    row.put("subject", permission.getCode());
                    row.put("action", action.replace("[", "").replace("]", ""));
                    list.add(row);
                }
            }
        });
        return list;

    }

    public static List<ShowPermissionResponse> getRoleAbilities(int roleId)
    {
        List<ShowPermissionResponse> showRoleResponseList = new ArrayList<>();
        List<PgPermission> pgPermissions = pgPermissionRepository.findAll();
        pgPermissions.forEach(pgPermission -> {
            Optional<PgRolePermissionMap> pgRolePermissionMap = pgRolePermissionMapRepository.findByRoleIdAndPermissionId(roleId, pgPermission.getId());
            ShowPermissionResponse permissionResponse = new ShowPermissionResponse();
            permissionResponse.setId(pgPermission.getId());
            permissionResponse.setName(pgPermission.getName());
            permissionResponse.setCode(pgPermission.getCode());
            permissionResponse.setActions(pgPermission.getActions());
            permissionResponse.setAvailableActions(CoreBase.getAvailableActions(pgPermission.getActions()));
            if (pgRolePermissionMap.isPresent()) {
                permissionResponse.setPermissionAction(CoreBase.getAvailableActions(pgRolePermissionMap.get().getActions()));
            }else {
                permissionResponse.setPermissionAction(Collections.emptyList().toArray(new String[0]));
            }
            showRoleResponseList.add(permissionResponse);
        });
        return showRoleResponseList;
    }

    public static String getSystemParamByKey(String key)
    {
        return Objects.requireNonNull(pgSystemParamRepository.findTopByKey(key).orElse(null)).getValue();
    }

    public static String encryptPassword(String password)
    {
        return passwordEncoder.encode(password);
    }

}
