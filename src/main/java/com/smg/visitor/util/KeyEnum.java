package com.smg.visitor.util;

import org.springframework.stereotype.Component;

@Component
public interface KeyEnum {
    String DATA_COLUMN_KEY = "0123456789123456";

    long accessTokenLifeTime = 100;
    long refreshTokenLifeTime = 1000;

}
