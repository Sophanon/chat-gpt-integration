package com.smg.visitor.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.HashMap;

public class ObjectConverter {

    public static Object convertJsonToObject(String json, Class<?> type) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        Object data = mapper.readValue(json, type);
        return data;
    }

    public static Object convertToObject(Object object, Class<?> type)  {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        Object data = mapper.convertValue(object, type);
        return data;
    }

    public static Object converArraytToArrayObject(Object object, TypeReference type)  {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.registerModule(new JavaTimeModule());
        return mapper.convertValue(object, type);
    }

    public static String formatAmount(String value){
        Double data = Double.parseDouble(value);
        return String.format("%,.0f", data);
    }

    public static Object getData(HashMap values, String key, Class t) {
        return values.get(key) != null ? ObjectConverter.convertToObject(String.valueOf(values.get(key)), t) : null;
    }


}
