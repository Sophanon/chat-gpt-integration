package com.smg.visitor.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Component
public class EncryptDecryptProvider {

    private ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> map = new HashMap<>();

    @Value("${aes.key}")
    private String key;

    @Value("${aes.iv}")
    private String initVector;

    @Value("${PASS.key}")
    private String keyPassword;

    @Value("${PASS.iv}")
    private String initVectorPassword;

    @Value("${AD.key}")
    private String AD_key;

    @Value("${AD.iv}")
    private String AD_initVector;



    public String Encrypt(String text) {
        try {
            SecretKeySpec skeySpec = this.generateKey(key);
            IvParameterSpec iv = this.generateIV(initVector);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(text.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception e) {
            System.out.println(">>> Encrypt: " + e.getMessage());
            return "";
        }
    }

    public String Decrypt(String encrypted) {
        try {
            SecretKeySpec skeySpec = this.generateKey(key);
            IvParameterSpec iv = this.generateIV(initVector);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
            return new String(original);
        } catch (Exception e) {
            System.out.println(">>> Decrypt: " + e.getMessage());
            return "";
        }
    }

    public String AD_Encrypt(String text) {
        try {
            IvParameterSpec iv = new IvParameterSpec(AD_initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(AD_key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(text.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception e) {
            return "";
        }
    }

    public String AD_Decrypt(String encrypted) {
        try {

            IvParameterSpec iv = new IvParameterSpec(AD_initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(AD_key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
            return new String(original);
        } catch (Exception e) {
            return "";
        }
    }

    public String EncryptPassword(String text) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVectorPassword.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(keyPassword.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(text.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception e) {
            return "";
        }
    }

    public String DecryptPassword(String encrypted) {
        try {

            IvParameterSpec iv = new IvParameterSpec(initVectorPassword.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(keyPassword.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
            return new String(original);
        } catch (Exception e) {
            return "";
        }
    }

    public SecretKeySpec generateKey(String key) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(key.getBytes());
        SecretKeySpec secretKeySpec = new SecretKeySpec(md.digest(), "AES");
        return secretKeySpec;
    }

    public IvParameterSpec generateIV(String iv) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(iv.getBytes());

        byte[] bytes = md.digest();
        byte[] s = Arrays.copyOfRange(bytes, 0, 16);

        IvParameterSpec ivParameterSpec = new IvParameterSpec(s);
        return ivParameterSpec;
    }

//    public IvParameterSpec generateIV(String iv) throws NoSuchAlgorithmException {
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        md.update(iv.getBytes());
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(md.digest());
//        return ivParameterSpec;
//    }

}
