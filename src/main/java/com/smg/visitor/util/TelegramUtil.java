package com.smg.visitor.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

@Component
public class TelegramUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramUtil.class);

    @Value("${telegram.url}")
    private String telegramUrl;

    @Value("${telegram.chat.id}")
    private String chatId;

    @Value("${spring.profiles.active}")
    String activeProfile;

    @Async
    public CompletableFuture<Void> sendMessage(int statusCode, String message, String action, String route) {
        if (statusCode == 401) {
            return CompletableFuture.completedFuture(null);
        }

        HashMap<String, Object> payload = new HashMap<>();
        payload.put("chat_id", chatId);
        String param = "";
        param += "ENV: *" + String.format(activeProfile.toUpperCase()) + "*" ;
        param += String.format(" %nRoute: " + route);
        param += String.format(" %nAction: " + action);
        param += String.format(" %nResponse: " + message);
        param += String.format(" %nStatus Code: " + statusCode);
        param += String.format(" %nTime: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString());
        payload.put("text", param);
        WebClient webClient = WebClient.builder().build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Mono<Void> response = webClient.post()
                .uri(telegramUrl)
                .headers(httpHeaders -> httpHeaders.addAll(headers))
                .body(BodyInserters.fromValue(payload))
                .retrieve()
                .bodyToMono(String.class)
                .then();
        return response.toFuture();
    }

    @Async
    public CompletableFuture<String> send(Object data)
    {
        HashMap<String, Object> payload = new HashMap<>();
        payload.put("chat_id", chatId);
        payload.put("text", data);
        WebClient webClient = WebClient.builder().build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Mono<String> responseMono = webClient.post()
                .uri(telegramUrl)
                .headers(httpHeaders -> httpHeaders.addAll(headers))
                .body(BodyInserters.fromValue(payload))
                .retrieve()
                .bodyToMono(String.class);
        responseMono.subscribe(response -> {
            // Handle the response here
            System.out.println("Response: " + response);
        }, error -> {
            // Handle any errors that occur
            System.err.println("Error: " + error);
        });
        return null;
    }

    @Async
    public CompletableFuture<String> send(Object data, String groupChatId)
    {
        HashMap<String, Object> payload = new HashMap<>();
        payload.put("chat_id", groupChatId);
        payload.put("text", data);
        WebClient webClient = WebClient.builder().build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Mono<String> responseMono = webClient.post()
                .uri(telegramUrl)
                .headers(httpHeaders -> httpHeaders.addAll(headers))
                .body(BodyInserters.fromValue(payload))
                .retrieve()
                .bodyToMono(String.class);
        responseMono.subscribe(response -> {
            // Handle the response here
            System.out.println("Response: " + response);
        }, error -> {
            // Handle any errors that occur
            System.err.println("Error: " + error);
        });
        return null;
    }

}
