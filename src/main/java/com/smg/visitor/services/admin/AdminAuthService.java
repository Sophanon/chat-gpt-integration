package com.smg.visitor.services.admin;

import com.smg.visitor.config.exception.ResponseException;
import com.smg.visitor.dto.request.admin.auth.AdminAuthLoginRequest;
import com.smg.visitor.dto.request.admin.auth.AdminAuthRefreshRequest;
import com.smg.visitor.dto.request.admin.auth.AuthProfileRequest;
import com.smg.visitor.dto.request.admin.profile.UpdateProfilePasswordRequest;
import com.smg.visitor.dto.request.admin.profile.UpdateProfileRequest;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.dto.response.admin.auth.AuthLoginResponse;
import com.smg.visitor.dto.response.admin.auth.UserProfileResponse;
import com.smg.visitor.postgressource.entity.PgUser;
import com.smg.visitor.postgressource.entity.PgUserLoginAccess;
import com.smg.visitor.postgressource.repository.PgUserLoginAccessRepository;
import com.smg.visitor.postgressource.repository.PgUserRepository;
import com.smg.visitor.util.CoreBase;
import com.smg.visitor.util.EncryptDecryptProvider;
import com.smg.visitor.util.KeyEnum;
import com.smg.visitor.util.PSUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AdminAuthService {

    @Autowired
    private PgUserRepository pgUserRepository;
    @Autowired
    private PgUserLoginAccessRepository pgUserLoginAccessRepository;
    @Autowired
    private EncryptDecryptProvider encryptDecryptProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional(rollbackFor = ResponseException.class)
    public AuthLoginResponse login(AdminAuthLoginRequest request) throws Exception
    {
        Optional<PgUser> pgUserOptional = pgUserRepository.findTopByEmailOrUserName(request.getEmail(), request.getEmail());
        if (pgUserOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ACCOUNT_NOT_FOUND);
        }
        if (!pgUserOptional.get().isEnable()) {
            throw new ResponseException(ResponseMessage.ACCOUNT_BLOCK);
        }
        if (!passwordEncoder.matches(request.getPassword(), pgUserOptional.get().getPassword())) {
            throw new ResponseException(ResponseMessage.BAD_CREDENTIAL);
        }
        PgUserLoginAccess pgUserLoginAccess = new PgUserLoginAccess();
        pgUserLoginAccessRepository.updateRevokeToFalseByUserId(pgUserLoginAccess.getUserId());
        pgUserLoginAccess.setUserId(pgUserOptional.get().getId());
        return getObject(pgUserLoginAccess);
    }

    public PgUser verifyAuthToken(String token) throws UnknownHostException
    {
        Optional<PgUserLoginAccess> pgUserLoginAccessOptional = pgUserLoginAccessRepository.findTopByAccessTokenAndRevokeAndAccessTokenExpiredAtAfter(token, false, LocalDateTime.now());
        if (pgUserLoginAccessOptional.isPresent()) {
            Optional<PgUser> pgUserOptional = pgUserRepository.findById(pgUserLoginAccessOptional.get().getUserId());
            if (pgUserOptional.isPresent() && pgUserOptional.get().getDeletedAt() == null && pgUserOptional.get().isEnable()) {
                PgUserLoginAccess  pgUserLoginAccess = pgUserLoginAccessOptional.get();
                this.getTokenExpired(pgUserLoginAccess);
                pgUserLoginAccessRepository.save(pgUserLoginAccess);
                return pgUserOptional.get();
            }
        }
        return null;
    }

    public AuthLoginResponse refreshToken(AdminAuthRefreshRequest request) throws Exception {

        String refreshToken = encryptDecryptProvider.Decrypt(request.getToken());
        String[] refreshData = refreshToken.split("-");
        if (refreshData.length > 1) {
            Optional<PgUserLoginAccess> pgUserLoginAccessOptional = pgUserLoginAccessRepository.findTopByRefreshTokenAndRevokeAndRefreshTokenExpiredAtAfter(refreshData[0], false, LocalDateTime.now());
            if (pgUserLoginAccessOptional.isPresent()) {
                PgUserLoginAccess pgUserLoginAccess = pgUserLoginAccessOptional.get();
                Optional<PgUser> pgUserOptional = pgUserRepository.findById(pgUserLoginAccess.getUserId());
                if (pgUserOptional.isPresent() && pgUserOptional.get().getDeletedAt() == null && pgUserOptional.get().isEnable()) {
                    return getObject(pgUserLoginAccess);
                }
            }
        }
        return null;
    }

    private AuthLoginResponse getObject(PgUserLoginAccess pgUserLoginAccess) throws Exception {
        pgUserLoginAccess.setAccessToken(PSUUID.generateToken());
        pgUserLoginAccess.setRefreshToken(PSUUID.generateToken());
        this.getTokenExpired(pgUserLoginAccess);
        pgUserLoginAccessRepository.save(pgUserLoginAccess);
        return new AuthLoginResponse(pgUserLoginAccess);
    }

    private PgUserLoginAccess getTokenExpired(PgUserLoginAccess pgUserLoginAccess)
    {
        String accessTokenExpiredAt = CoreBase.getSystemParamByKey("access_token_expired_at");
        if (accessTokenExpiredAt != null) {
            pgUserLoginAccess.setAccessTokenExpiredAt(LocalDateTime.now().plusMinutes(Long.parseLong(accessTokenExpiredAt)));
        }else {
            pgUserLoginAccess.setAccessTokenExpiredAt(LocalDateTime.now().plusMinutes(KeyEnum.accessTokenLifeTime));
        }
        String refreshTokenExpiredAt = CoreBase.getSystemParamByKey("refresh_token_expired_at");
        if (refreshTokenExpiredAt != null) {
            pgUserLoginAccess.setRefreshTokenExpiredAt(LocalDateTime.now().plusMinutes(Long.parseLong(refreshTokenExpiredAt)));
        }else {
            pgUserLoginAccess.setRefreshTokenExpiredAt(LocalDateTime.now().plusMinutes(KeyEnum.refreshTokenLifeTime));
        }
        return pgUserLoginAccess;
    }

    public UserProfileResponse getProfile() throws Exception {
        AuthProfileRequest authProfile = CoreBase.authProvider.getAuth();
        Optional<PgUser> pgUserOptional = pgUserRepository.findById(authProfile.getId());
        return pgUserOptional.map(UserProfileResponse::new).orElse(null);
    }

    public void logout(AdminAuthRefreshRequest request) throws Exception
    {
        String token = encryptDecryptProvider.Decrypt(request.getToken());
        String[] tokenData = token.split("-");
        if (tokenData.length > 1) {
            Optional<PgUserLoginAccess> pgUserLoginAccessOptional = pgUserLoginAccessRepository.findTopByAccessTokenAndRevokeAndAccessTokenExpiredAtAfter(tokenData[0], false, LocalDateTime.now());
            if (pgUserLoginAccessOptional.isPresent()) {
                PgUserLoginAccess pgUserLoginAccess = pgUserLoginAccessOptional.get();
                pgUserLoginAccess.setRevoke(true);
                pgUserLoginAccessRepository.save(pgUserLoginAccess);
            }
        }
    }

    public UserProfileResponse updateProfile(UpdateProfileRequest request) throws Exception {
        AuthProfileRequest authProfile = CoreBase.authProvider.getAuth();
        Optional<PgUser> pgUserOptional = pgUserRepository.findById(authProfile.getId());
        if (pgUserOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        PgUser pgUser = pgUserOptional.get();
        pgUser.setName(request.getName());
        pgUserRepository.save(pgUser);
        return new UserProfileResponse(pgUser);
    }

    public UserProfileResponse updatePassword(UpdateProfilePasswordRequest request) throws Exception {
        AuthProfileRequest authProfile = CoreBase.authProvider.getAuth();
        Optional<PgUser> pgUserOptional = pgUserRepository.findById(authProfile.getId());
        if (pgUserOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        PgUser pgUser = pgUserOptional.get();
        if (!pgUser.getPassword().contentEquals(request.getOldPassword())) {
            throw new ResponseException(ResponseMessage.OLD_PASSWORD_NOT_CORRECT);
        }
        pgUser.setPassword(request.getPassword());
        pgUserRepository.save(pgUser);
        return new UserProfileResponse(pgUser);
    }


}
