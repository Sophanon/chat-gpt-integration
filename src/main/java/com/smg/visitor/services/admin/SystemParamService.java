package com.smg.visitor.services.admin;

import com.smg.visitor.dto.request.FilterPaging;
import com.smg.visitor.dto.request.admin.system_param.SystemParamPagingRequest;
import com.smg.visitor.dto.request.admin.system_param.UpdateSystemParamRequest;
import com.smg.visitor.dto.response.ListResponse;
import com.smg.visitor.dto.response.admin.system_param.ListSystemParamResponse;
import com.smg.visitor.postgressource.entity.PgSystemParam;
import com.smg.visitor.postgressource.repository.PgSystemParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.swing.text.html.Option;
import java.lang.module.ResolutionException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SystemParamService {

    @Autowired
    private PgSystemParamRepository pgSystemParamRepository;


    public ListResponse index(SystemParamPagingRequest pagingRequest) throws Exception
    {
        Sort sort = Sort.by(Sort.Direction.fromString(pagingRequest.getSort()), pagingRequest.getSortBy());
        // Pagination
        Pageable page = PageRequest.of(pagingRequest.getPage(), pagingRequest.getLimit(), sort);
        Specification<PgSystemParam> filter = this.filterRoleColumn(pagingRequest);
        Page<PgSystemParam> systemParams = pgSystemParamRepository.findAll(filter, page);

        Page<ListSystemParamResponse> listSystemParamResponses = systemParams.map(systemParam -> {
            return new ListSystemParamResponse(systemParam);
        });
        return new ListResponse(listSystemParamResponses.getContent(), systemParams.getTotalElements());
    }

    @Transactional(rollbackFor = ResolutionException.class)
    public void update(UpdateSystemParamRequest request) throws Exception
    {
        Optional<PgSystemParam> systemParamOptional = pgSystemParamRepository.findTopByKey(request.getKey());
        if (systemParamOptional.isPresent()) {
            PgSystemParam pgSystemParam = systemParamOptional.get();
            pgSystemParam.setValue(request.getValue());
            pgSystemParam.setKey(request.getKey());
            pgSystemParamRepository.save(pgSystemParam);
        }
    }

    public Specification<PgSystemParam> filterRoleColumn(SystemParamPagingRequest pagingRequest) {
        Specification<PgSystemParam> specification = new Specification<PgSystemParam>() {
            @Override
            public Predicate toPredicate(Root<PgSystemParam> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                for (FilterPaging searchRequest : pagingRequest.getFilters()) {
                    if (Objects.equals(searchRequest.getSearchBy(), "deletedAt")) {
                        predicates.add(criteriaBuilder.isNull(root.get(searchRequest.getSearchBy())));
                    } else if (searchRequest.getSearchOperator().equals("%")) {
                        predicates.add(criteriaBuilder.like(root.get(searchRequest.getSearchBy()), "%" + searchRequest.getSearch() + "%"));
                    } else if (searchRequest.getSearchOperator().equals("=")) {
                        predicates.add(criteriaBuilder.equal(root.get(searchRequest.getSearchBy()), searchRequest.getSearch()));
                    }
                }
                if (pagingRequest.getFilterType().equals("OR"))
                    return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                else
                    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }
        };

        return specification;
    }

}
