package com.smg.visitor.services.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.smg.visitor.config.exception.ResponseException;
import com.smg.visitor.dto.request.FilterPaging;
import com.smg.visitor.dto.request.ShowRequest;
import com.smg.visitor.dto.request.StatusRequest;
import com.smg.visitor.dto.request.admin.role.RolePagingRequest;
import com.smg.visitor.dto.request.admin.role.StorePermissionRequest;
import com.smg.visitor.dto.request.admin.role.StoreRoleRequest;
import com.smg.visitor.dto.request.admin.role.UpdateRoleRequest;
import com.smg.visitor.dto.response.ListResponse;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.dto.response.admin.role.ListPermissionResponse;
import com.smg.visitor.dto.response.admin.role.ListRoleResponse;
import com.smg.visitor.dto.response.admin.role.ShowRoleResponse;
import com.smg.visitor.postgressource.entity.PgPermission;
import com.smg.visitor.postgressource.entity.PgRole;
import com.smg.visitor.postgressource.entity.PgRolePermissionMap;
import com.smg.visitor.postgressource.repository.PgPermissionRepository;
import com.smg.visitor.postgressource.repository.PgRolePermissionMapRepository;
import com.smg.visitor.postgressource.repository.PgRoleRepository;
import com.smg.visitor.util.CoreBase;
import com.smg.visitor.util.ObjectConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    private PgRoleRepository pgRoleRepository;
    @Autowired
    private PgPermissionRepository pgPermissionRepository;
    @Autowired
    private PgRolePermissionMapRepository pgRolePermissionMapRepository;

    public ListResponse listForAdmin(RolePagingRequest pagingRequest) throws Exception {
        return getListResponse(pagingRequest);
    }

    public ListResponse options(RolePagingRequest pagingRequest) throws Exception {
        FilterPaging filterPaging = new FilterPaging("is_enable", "=", true);
        pagingRequest.getFilters().add(filterPaging);
        return getListResponse(pagingRequest);
    }

    private ListResponse getListResponse(RolePagingRequest pagingRequest) throws Exception {
        Sort sort = Sort.by(Sort.Direction.fromString(pagingRequest.getSort()), pagingRequest.getSortBy());
        // Pagination
        Pageable page = PageRequest.of(pagingRequest.getPage(), pagingRequest.getLimit(), sort);
        Specification<PgRole> filter = this.filterRoleColumn(pagingRequest);
        Page<PgRole> roles = pgRoleRepository.findAll(filter, page);

        Page<ListRoleResponse> listRoleResponses = roles.map(role -> {
            return new ListRoleResponse(role);
        });
        return new ListResponse(listRoleResponses.getContent(), roles.getTotalElements());
    }

    @Transactional(rollbackFor = ResponseException.class)
    public ShowRoleResponse store(StoreRoleRequest request) throws Exception
    {
        Optional<PgRole> pgRoleOptional = pgRoleRepository.findByName(request.getName());
        if (pgRoleOptional.isPresent()) {
            throw new ResponseException(ResponseMessage.ROLE_ALREADY_EXIST);
        }
        PgRole pgRole = new PgRole();
        return getShowRoleResponse(pgRole, request.getName(), request.getDescription(), request.isEnable(), request.getAbilities());
    }

    @Transactional(rollbackFor = ResponseException.class)
    public ShowRoleResponse update(UpdateRoleRequest request) throws Exception
    {
        Optional<PgRole> pgRoleOptional = pgRoleRepository.findById(request.getId());
        if (pgRoleOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        Optional<PgRole> pgRoleOptional1 = pgRoleRepository.findByNameAndIdNot(request.getName(), request.getId());
        if (pgRoleOptional1.isPresent()) {
            throw new ResponseException(ResponseMessage.ROLE_ALREADY_EXIST);
        }
        PgRole pgRole = pgRoleOptional.get();
        return getShowRoleResponse(pgRole, request.getName(), request.getDescription(), request.isEnable(), request.getAbilities());
    }

    @Transactional(rollbackFor = ResponseException.class)
    public ShowRoleResponse updateStatus(StatusRequest request) throws Exception
    {
        Optional<PgRole> pgRoleOptional = pgRoleRepository.findById(request.getId());
        if (pgRoleOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        PgRole pgRole = pgRoleOptional.get();
        pgRole.setEnable(request.isEnable());
        pgRoleRepository.save(pgRole);
        return new ShowRoleResponse(pgRole);
    }

    private ShowRoleResponse getShowRoleResponse(PgRole pgRole, String name, String description, boolean enable, List<StorePermissionRequest> abilities) {
        pgRole.setName(name);
        pgRole.setDescription(description);
        pgRole.setEnable(enable);
        pgRoleRepository.save(pgRole);
        pgRolePermissionMapRepository.deleteAllByRoleId(pgRole.getId());
        abilities.forEach(ability -> {
            StringBuilder action = new StringBuilder();
            int index = 0;
            for (String act : ability.getPermissionAction()) {
                if (index > 0) {
                    action.append("|");
                }
                action.append("[").append(act).append("]");
                index++;
            }

            if (!action.toString().isBlank()) {
                PgRolePermissionMap pgRolePermissionMap = new PgRolePermissionMap();
                pgRolePermissionMap.setRoleId(pgRole.getId());
                pgRolePermissionMap.setPermissionId(ability.getId());
                pgRolePermissionMap.setActions(action.toString());
                pgRolePermissionMapRepository.save(pgRolePermissionMap);
            }
        });
        return new ShowRoleResponse(pgRole);
    }

    public ShowRoleResponse show(ShowRequest request) throws Exception
    {
        Optional<PgRole> pgRoleOptional = pgRoleRepository.findById(request.getId());
        if (pgRoleOptional.isEmpty()) {
            throw new ResponseException(404, ResponseMessage.ITEM_NOT_FOUND);
        }
        return new ShowRoleResponse(pgRoleOptional.get());
    }

    public void delete(ShowRequest request) throws Exception
    {
        pgRoleRepository.softDelete(request.getId(), LocalDateTime.now(), CoreBase.authProvider.getAuth().getId());
    }

    // Filter Function : return as Specification Class
    public Specification<PgRole> filterRoleColumn(RolePagingRequest pagingRequset) {
        Specification<PgRole> specification = new Specification<PgRole>() {
            @Override
            public Predicate toPredicate(Root<PgRole> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                for (FilterPaging searchRequest : pagingRequset.getFilters()) {
                    if (Objects.equals(searchRequest.getSearchBy(), "deletedAt")) {
                        predicates.add(criteriaBuilder.isNull(root.get(searchRequest.getSearchBy())));
                    } else if (searchRequest.getSearchOperator().equals("%")) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(searchRequest.getSearchBy())),
                                "%" + searchRequest.getSearch().toString().toLowerCase() + "%"));
                    } else if (searchRequest.getSearchOperator().equals("=")) {
                        predicates.add(criteriaBuilder.equal(root.get(searchRequest.getSearchBy()), searchRequest.getSearch()));
                    }
                }
                if (pagingRequset.getFilterType().equals("OR"))
                    return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                else
                    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }
        };

        return specification;
    }

    public Object permissions() throws Exception
    {
        Sort sort = Sort.by(Sort.Direction.fromString("ASC"), "sequenceOrder");
        List<PgPermission> permissions = pgPermissionRepository.findAll(sort);
        return (List<ListPermissionResponse>) ObjectConverter.converArraytToArrayObject(permissions, new TypeReference<List<ListPermissionResponse>>() {
        });
    }
}
