package com.smg.visitor.services.admin;

import com.smg.visitor.config.exception.ResponseException;
import com.smg.visitor.dto.request.FilterPaging;
import com.smg.visitor.dto.request.ListRequest;
import com.smg.visitor.dto.request.ShowRequest;
import com.smg.visitor.dto.request.admin.money_flow.StoreMoneyFlowRequest;
import com.smg.visitor.dto.request.admin.money_flow.UpdateMoneyFlowRequest;
import com.smg.visitor.dto.response.ListResponse;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.dto.response.admin.money_flow.MoneyFlowResponse;
import com.smg.visitor.postgressource.entity.PgMoneyFlow;
import com.smg.visitor.postgressource.repository.PgMoneyFlowRepository;
import com.smg.visitor.util.CoreBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class MoneyFlowService {

    @Autowired
    private PgMoneyFlowRepository pgMoneyFlowRepository;

    public ListResponse list(ListRequest pagingRequest) throws Exception
    {
        Sort sort = Sort.by(Sort.Direction.fromString(pagingRequest.getSort()), pagingRequest.getSortBy());
        // Pagination
        Pageable page = PageRequest.of(pagingRequest.getPage(), pagingRequest.getLimit(), sort);
        Specification<PgMoneyFlow> filter = this.filterUserColumn(pagingRequest);
        Page<PgMoneyFlow> moneyFlows = pgMoneyFlowRepository.findAll(filter, page);

        Page<MoneyFlowResponse> listRoleResponses = moneyFlows.map(moneyFlow -> {
            return new MoneyFlowResponse(moneyFlow);
        });
        return new ListResponse(listRoleResponses.getContent(), moneyFlows.getTotalElements());
    }

    public Specification<PgMoneyFlow> filterUserColumn(ListRequest pagingRequest) {
        Specification<PgMoneyFlow> specification = new Specification<PgMoneyFlow>() {
            @Override
            public Predicate toPredicate(Root<PgMoneyFlow> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                for (FilterPaging searchRequest : pagingRequest.getFilters()) {
                    if (Objects.equals(searchRequest.getSearchBy(), "deletedAt")) {
                        predicates.add(criteriaBuilder.isNull(root.get(searchRequest.getSearchBy())));
                    } else if (searchRequest.getSearchOperator().equals("%")) {
                        predicates.add(criteriaBuilder.like(root.get(searchRequest.getSearchBy()), "%" + searchRequest.getSearch() + "%"));
                    } else if (searchRequest.getSearchOperator().equals("=")) {
                        predicates.add(criteriaBuilder.equal(root.get(searchRequest.getSearchBy()), searchRequest.getSearch()));
                    }
                }
                if (pagingRequest.getFilterType().equals("OR"))
                    return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                else
                    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }
        };

        return specification;
    }

    public MoneyFlowResponse show(ShowRequest request) throws Exception
    {
        Optional<PgMoneyFlow> pgOptional = pgMoneyFlowRepository.findById(request.getId());
        if (pgOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        return new MoneyFlowResponse(pgOptional.get());
    }

    public MoneyFlowResponse store(StoreMoneyFlowRequest request) throws Exception
    {
        PgMoneyFlow pgMoneyFlow = new PgMoneyFlow();
        return object(pgMoneyFlow, request.getDescription(), request.getType(), request.getAmount(), request.getDate());
    }

    public MoneyFlowResponse update(UpdateMoneyFlowRequest request) throws Exception
    {
        Optional<PgMoneyFlow> pgOptional = pgMoneyFlowRepository.findById(request.getId());
        if (pgOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        return object(pgOptional.get(), request.getDescription(), request.getType(), request.getAmount(), request.getDate());
    }

    public MoneyFlowResponse object(PgMoneyFlow pgMoneyFlow, String description, String type, double amount, LocalDate date)
    {
        pgMoneyFlow.setType(type);
        pgMoneyFlow.setDescription(description);
        pgMoneyFlow.setAmount(amount);
        pgMoneyFlow.setDate(date);
        pgMoneyFlowRepository.save(pgMoneyFlow);
        return new MoneyFlowResponse(pgMoneyFlow);
    }

    public void delete(ShowRequest request) throws Exception
    {
        pgMoneyFlowRepository.softDelete(request.getId(), LocalDateTime.now(), CoreBase.authProvider.getAuth().getId());
    }

    public Object types()
    {
        ArrayList<HashMap<String, Object>> types = new ArrayList<>();
        HashMap<String, Object> type = new HashMap<>();
        type.put("type", "Income");
        types.add(type);
        type.put("type", "Expense");
        types.add(type);
        return types;
    }


}
