package com.smg.visitor.services.admin;

import com.smg.visitor.config.exception.ResponseException;
import com.smg.visitor.dto.request.FilterPaging;
import com.smg.visitor.dto.request.ShowRequest;
import com.smg.visitor.dto.request.StatusRequest;
import com.smg.visitor.dto.request.admin.user.StoreRoleRequest;
import com.smg.visitor.dto.request.admin.user.StoreUserRequest;
import com.smg.visitor.dto.request.admin.user.UpdateUserRequest;
import com.smg.visitor.dto.request.admin.user.UserPagingRequest;
import com.smg.visitor.dto.response.ListResponse;
import com.smg.visitor.dto.response.ResponseMessage;
import com.smg.visitor.dto.response.admin.user.ListUserResponse;
import com.smg.visitor.dto.response.admin.user.ShowUserResponse;
import com.smg.visitor.postgressource.entity.PgUser;
import com.smg.visitor.postgressource.entity.PgUserRoleMap;
import com.smg.visitor.postgressource.repository.PgUserRepository;
import com.smg.visitor.postgressource.repository.PgUserRoleMapRepository;
import com.smg.visitor.util.CoreBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private PgUserRepository pgUserRepository;
    @Autowired
    private PgUserRoleMapRepository pgUserRoleMapRepository;


    public ListResponse listForUser(UserPagingRequest pagingRequest) throws Exception
    {
        Sort sort = Sort.by(Sort.Direction.fromString(pagingRequest.getSort()), pagingRequest.getSortBy());
        // Pagination
        Pageable page = PageRequest.of(pagingRequest.getPage(), pagingRequest.getLimit(), sort);
        Specification<PgUser> filter = this.filterUserColumn(pagingRequest);
        Page<PgUser> roles = pgUserRepository.findAll(filter, page);

        Page<ListUserResponse> listRoleResponses = roles.map(user -> {
            return new ListUserResponse(user);
        });
        return new ListResponse(listRoleResponses.getContent(), roles.getTotalElements());
    }

    public Specification<PgUser> filterUserColumn(UserPagingRequest pagingRequest) {
        Specification<PgUser> specification = new Specification<PgUser>() {
            @Override
            public Predicate toPredicate(Root<PgUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                for (FilterPaging searchRequest : pagingRequest.getFilters()) {
                    if (Objects.equals(searchRequest.getSearchBy(), "deletedAt")) {
                        predicates.add(criteriaBuilder.isNull(root.get(searchRequest.getSearchBy())));
                    } else if (searchRequest.getSearchOperator().equals("%") && searchRequest.getSearchBy().equals("search")) {
                        predicates.add(criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.lower(root.get("userName")), "%" + searchRequest.getSearch().toString().toLowerCase() + "%"),
                                criteriaBuilder.like(criteriaBuilder.lower(root.get("email")), "%" + searchRequest.getSearch().toString().toLowerCase() + "%"),
                                criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + searchRequest.getSearch().toString().toLowerCase() + "%")));
                    } else if (searchRequest.getSearchOperator().equals("%")) {
                        predicates.add(criteriaBuilder.like(root.get(searchRequest.getSearchBy()), "%" + searchRequest.getSearch() + "%"));
                    } else if (searchRequest.getSearchOperator().equals("=")) {
                        predicates.add(criteriaBuilder.equal(root.get(searchRequest.getSearchBy()), searchRequest.getSearch()));
                    }
                }
                if (pagingRequest.getFilterType().equals("OR"))
                    return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                else
                    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }
        };

        return specification;
    }

    public ShowUserResponse show(ShowRequest request) throws Exception
    {
        Optional<PgUser> pgUserOptional = pgUserRepository.findById(request.getId());
        if (pgUserOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        return new ShowUserResponse(pgUserOptional.get());
    }

    @Transactional(rollbackFor = ResponseException.class)
    public ShowUserResponse store(StoreUserRequest request) throws Exception
    {
        Optional<PgUser> pgUserOptional = pgUserRepository.findTopByEmail(request.getEmail());
        if (pgUserOptional.isPresent()) {
            throw new ResponseException(ResponseMessage.EMAIL_ALREADY_EXIST);
        }

        Optional<PgUser> pgUserOptional2 = pgUserRepository.findTopByUserName(request.getUserName());
        if (pgUserOptional2.isPresent()) {
            throw new ResponseException(ResponseMessage.USER_NAME_ALREADY_EXIST);
        }
        PgUser pgUser = new PgUser();
        return getShowUserResponse(pgUser, request.getName(),request.getUserName(), request.getEmail(),request.getPassword(),
                request.isEnable(), request.getRoles());
    }

    @Transactional(rollbackFor = ResponseException.class)
    public ShowUserResponse update(UpdateUserRequest request) throws Exception
    {
        Optional<PgUser> pgUserOptional = pgUserRepository.findById(request.getId());
        if (pgUserOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }

        Optional<PgUser> pgUserNameOptional = pgUserRepository.findTopByUserNameAndIdNot(request.getUserName(),request.getId());
        if (pgUserNameOptional.isPresent()) {
            throw new ResponseException(ResponseMessage.USER_NAME_ALREADY_EXIST);
        }

        PgUser pgUser = pgUserOptional.get();
        return getShowUserResponse(pgUser, request.getName(), request.getUserName(),request.getEmail(),request.getPassword(),
                request.isEnable(), request.getRoles());
    }

    @Transactional(rollbackFor = ResponseException.class)
    public ShowUserResponse updateStatus(StatusRequest request) throws Exception
    {
        Optional<PgUser> pgUserOptional = pgUserRepository.findById(request.getId());
        if (pgUserOptional.isEmpty()) {
            throw new ResponseException(ResponseMessage.ITEM_NOT_FOUND);
        }
        if (pgUserOptional.get().getId() == CoreBase.authProvider.getAuthUser().getId()) {
            throw new ResponseException(ResponseMessage.CANNOT_TURN_OFF);
        }
        PgUser pgUser = pgUserOptional.get();
        pgUser.setEnable(request.isEnable());
        pgUserRepository.save(pgUser);
        return new ShowUserResponse(pgUser);
    }

    private ShowUserResponse getShowUserResponse(PgUser pgUser, String name,String username, String email, String password,
                                                 boolean enable, List<StoreRoleRequest> roles)
            throws Exception {
        pgUser.setName(name);
        pgUser.setUserName(username);
        pgUser.setEmail(email);
        if (password != null && !password.equals("")) {
            pgUser.setPassword(password);
        }
        pgUser.setEnable(enable);
        pgUserRepository.save(pgUser);
        pgUserRoleMapRepository.deleteByUserId(pgUser.getId());
        roles.forEach(role -> {
            PgUserRoleMap pgUserRoleMap = new PgUserRoleMap();
            pgUserRoleMap.setUserId(pgUser.getId());
            pgUserRoleMap.setRoleId(role.getId());
            pgUserRoleMapRepository.save(pgUserRoleMap);
        });
        return new ShowUserResponse(pgUser);
    }
    public void delete(ShowRequest request) throws Exception
    {
        if (request.getId() == CoreBase.authProvider.getAuth().getId()) {
            throw new ResponseException(ResponseMessage.CANNOT_DELETE);
        }
        pgUserRepository.softDelete(request.getId(), LocalDateTime.now(), CoreBase.authProvider.getAuth().getId());
    }
}
