package com.smg.visitor.services;

import com.smg.visitor.dto.request.FilterPaging;
import com.smg.visitor.dto.request.file.FilePagingRequest;
import com.smg.visitor.dto.response.ListResponse;
import com.smg.visitor.dto.response.file.ListFileResponse;
import com.smg.visitor.postgressource.entity.PgMedia;
import com.smg.visitor.postgressource.repository.PgMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class FileService {
    
    @Autowired
    private PgMediaRepository pgMediaRepository;

    public ListResponse listForAdmin(FilePagingRequest pagingRequest) throws Exception {
        return getListResponse(pagingRequest);
    }

    private ListResponse getListResponse(FilePagingRequest pagingRequest) throws Exception {
        Sort sort = Sort.by(Sort.Direction.fromString(pagingRequest.getSort()), pagingRequest.getSortBy());
        // Pagination
        Pageable page = PageRequest.of(pagingRequest.getPage(), pagingRequest.getLimit(), sort);
        Specification<PgMedia> filter = this.filterColumn(pagingRequest);
        Page<PgMedia> files = pgMediaRepository.findAll(filter, page);

        Page<ListFileResponse> listfileResponses = files.map(file -> {
            return new ListFileResponse(file);
        });
        return new ListResponse(listfileResponses.getContent(), files.getTotalElements());
    }

    public Specification<PgMedia> filterColumn(FilePagingRequest pagingRequset) {
        Specification<PgMedia> specification = new Specification<PgMedia>() {
            @Override
            public Predicate toPredicate(Root<PgMedia> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                for (FilterPaging searchRequest : pagingRequset.getFilters()) {
                    if (Objects.equals(searchRequest.getSearchBy(), "deletedAt")) {
                        predicates.add(criteriaBuilder.isNull(root.get(searchRequest.getSearchBy())));
                    } else if (searchRequest.getSearchOperator().equals("%")) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(searchRequest.getSearchBy())),
                                "%" + searchRequest.getSearch().toString().toLowerCase() + "%"));
                    } else if (searchRequest.getSearchOperator().equals("=")) {
                        predicates.add(criteriaBuilder.equal(root.get(searchRequest.getSearchBy()), searchRequest.getSearch()));
                    }
                }
                if (pagingRequset.getFilterType().equals("OR"))
                    return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                else
                    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }
        };

        return specification;
    }
    
}
