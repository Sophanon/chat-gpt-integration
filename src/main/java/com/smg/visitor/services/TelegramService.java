package com.smg.visitor.services;

import com.smg.visitor.dto.response.chatgpt.ListChatGptMessageResponse;
import com.smg.visitor.postgressource.entity.PgChatTelegram;
import com.smg.visitor.postgressource.repository.PgChatTelegramRepository;
import com.smg.visitor.util.CoreBase;
import com.smg.visitor.util.TelegramUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.sql.Struct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Service
public class TelegramService {

    @Autowired
    private TelegramUtil telegramUtil;

    @Autowired
    private PgChatTelegramRepository pgChatTelegramRepository;

    @Value("${chatgpt.key}")
    private String chatGptKey;

    public void webhook(Map<String, Object> request) throws JSONException {
        String text;
        JSONObject jsonObject = new JSONObject(request);
        if (jsonObject.has("message")) {
            if (jsonObject.getJSONObject("message").has("new_chat_participant")) {
                if (jsonObject.getJSONObject("message").getJSONObject("new_chat_participant").getString("username").equals("sophanon_bot")) {
                    text = """
                            សួស្តីអ្នកទាំងអស់គ្នា ខ្ញុំរីករាយដែលបានឃើញអ្នកទាំងអស់គ្នានៅទីនេះ។
                            ABA: 001132038
                            https://link.payway.com.kh/aba?id=CCB1D55E3599&code=123626&acc=001132038
                            ស្រលាញ់អ្នកទាំងអស់គ្នាខ្លាំងណាស់។""";
                    telegramUtil.send(text, jsonObject.getJSONObject("message").getJSONObject("chat").getString("id"));
                }
            }else {
                if (jsonObject.getJSONObject("message").has("text")) {
                    if (jsonObject.getJSONObject("message").getJSONObject("from").getString("username").equals("ChhounSophanon") || jsonObject.getJSONObject("message").getJSONObject("from").getString("username").equals("othann168") || jsonObject.getJSONObject("message").getJSONObject("from").getString("username").equals("hiitssure")) {
                        String content = jsonObject.getJSONObject("message").getString("text");
                        PgChatTelegram pgChatTelegram = new PgChatTelegram();
                        pgChatTelegram.setRole("user");
                        pgChatTelegram.setContent(content);
                        pgChatTelegram.setTelegramChatId(jsonObject.getJSONObject("message").getJSONObject("chat").getString("id"));
                        pgChatTelegram.setMoney(false);
                        pgChatTelegramRepository.save(pgChatTelegram);
                        if (content.startsWith("@sophanon_bot")) {
                            this.sendToChatGpt(pgChatTelegram.getTelegramChatId());
                        }
                        if (content.contains("money")) {
                            text = "ABA: 001132038\n";
                            text += "https://link.payway.com.kh/aba?id=CCB1D55E3599&code=123626&acc=001132038";
                            telegramUtil.send(text, jsonObject.getJSONObject("message").getJSONObject("chat").getString("id"));
                        }
                    }
                }
            }
        }
    }

    @Async
    public void sendToChatGpt(String chatId)
    {
        HashMap<String, Object> payload = new HashMap<>();
        payload.put("model", "gpt-3.5-turbo");
        List<PgChatTelegram> pgChatTelegrams = pgChatTelegramRepository.findAllByTelegramChatId(chatId);
        List<ListChatGptMessageResponse> listChatGptMessageResponses = pgChatTelegrams.stream().map(ListChatGptMessageResponse::new).toList();
        payload.put("messages", listChatGptMessageResponses);
        WebClient webClient = WebClient.builder().build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + chatGptKey);
        Mono<String> responseMono = webClient.post()
                .uri(CoreBase.getSystemParamByKey("chatgpt_url"))
                .headers(httpHeaders -> httpHeaders.addAll(headers))
                .body(BodyInserters.fromValue(payload))
                .retrieve()
                .bodyToMono(String.class);
        responseMono.subscribe(response -> {
            // Handle the response here
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has("choices")) {
                    String content = "Hello! here is our answer from your question: " + pgChatTelegrams.get(pgChatTelegrams.size() - 1).getContent() + "\n";
                    content += jsonObject.getJSONArray("choices").getJSONObject(0).getJSONObject("message").getString("content");
                    PgChatTelegram pgChatTelegram = new PgChatTelegram();
                    pgChatTelegram.setRole("assistant");
                    pgChatTelegram.setContent(content);
                    pgChatTelegram.setTelegramChatId(chatId);
                    pgChatTelegram.setMoney(false);
                    pgChatTelegramRepository.save(pgChatTelegram);
                    telegramUtil.send(content, chatId);
                }
            } catch (JSONException e) {
                System.err.println("JSONException: "+ e.getMessage());
            }
        }, error -> {
            // Handle any errors that occur
            System.err.println("Error: " + error.getMessage());
        });
    }

}
