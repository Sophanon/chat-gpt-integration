package com.smg.visitor.services;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.smg.visitor.dto.response.file.ListFileResponse;
import com.smg.visitor.postgressource.entity.PgMedia;
import com.smg.visitor.postgressource.repository.PgMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@Service
public class S3Service {


    @Autowired
    private AmazonS3 amazonS3;

    @Autowired
    private PgMediaRepository pgMediaRepository;


    @Value("${amazon.aws.bucket-name}")
    private String bucketName;

    public ListFileResponse uploadImage(MultipartFile file) throws IOException, Exception {
        String fileName = file.getOriginalFilename();
        String extension = fileName.substring(fileName.lastIndexOf("."));
        String generatedFileName = "images/" + UUID.randomUUID().toString() + extension;
        File convertedFile = convertMultiPartFileToFile(file);
        amazonS3.putObject(new PutObjectRequest(bucketName, generatedFileName, convertedFile)
                .withCannedAcl(CannedAccessControlList.PublicRead));
        convertedFile.delete();
        amazonS3.getUrl(bucketName, generatedFileName).toString();
        return this.saveImage(fileName, extension, generatedFileName);
    }

    private File convertMultiPartFileToFile(MultipartFile file) throws IOException {
        File convertedFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convertedFile);
        fos.write(file.getBytes());
        fos.close();
        return convertedFile;
    }

    private ListFileResponse saveImage(String fileName, String extension, String path)
    {
        PgMedia pgMedia = new PgMedia();
        pgMedia.setExtension(extension);
        pgMedia.setName(fileName);
        pgMedia.setPath(path);
        pgMedia.setType("image");
        pgMediaRepository.save(pgMedia);
        return new ListFileResponse(pgMedia);
    }


}
