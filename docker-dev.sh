version=1.0.0;
mvn clean;
mvn package -P dev;
rsync -avzh --progress target/*.jar root@159.89.204.162:/var/www/chat-gpt-integration/target/;
ssh root@159.89.204.162 "
cd /var/www/chat-gpt-integration;
docker stop sophanon-api-microservice-dev;
docker build -f Dockerfile -t sophanon-api-microservice-dev-${version} .;
docker rm container -f sophanon-api-microservice-dev;
docker run -e "SPRING_PROFILES_ACTIVE=dev2" --restart=always -p 9099:8089 -d --name sophanon-api-microservice-dev sophanon-api-microservice-dev-${version};
docker system prune -f;
";