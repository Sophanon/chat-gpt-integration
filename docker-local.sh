mvn clean;
mvn package -P dev;
version=1.0.0;
docker stop pos-api | true;
docker build -f Dockerfile -t pos-api-${version} .
docker rm container -f pos-api | true;
#run docker spring normal
docker run -e "SPRING_PROFILES_ACTIVE=docker" --restart=always -p 9099:8089 -d --name pos-api pos-api-${version};